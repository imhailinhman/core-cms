$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function Main() {
}

Main.fn = {
    init: function () {
        Main.fn.initCheckbox.call(this);
        Main.fn.initdatetimepicker.call(this);
        Main.fn.initBulkActionChange.call(this);
        Main.fn.initItemActions.call(this);
        Main.fn.ininchecknumber.call(this);
        Main.fn.initPerPage.call(this);
        Main.fn.initFilter.call(this);
        Main.fn.initFilterDate.call(this);
        Main.fn.initActive.call(this);
        Main.fn.initimage.call(this);
        Main.fn.initdistricts.call(this);
    },

    initdatetimepicker: function () {

        $('#example').DataTable({
            destroy: true,
            "paging": false,
            "searching": false,
            "responsive": true,
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': 'nosort'
            }],
            "aaSorting": [[ 0, '' ]],

        });

        $('#datetimepicker').datepicker({
            viewMode: 'years'
        });


        var password = $('.password');
        password.prop('disabled', true);
        $('#changepassword').change(function () {
            if ($(this).is(":checked")) {
                $('#changepassword').val('on');
                password.removeAttr('disabled');
                password.prop('required', true);
                $("#confirm_password").prop('required', true)
            } else {
                $('#changepassword').val('off');
                $(".password").attr('disabled', '');
            }
        });
    },

    initimage: function () {
        $(document).on('change', '.btn-file :file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function (event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });


        function readURL(input, id_img) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    id_img.attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        var img_upload = $('#img-upload');
        $("#image").change(function () {
            readURL(this, img_upload);
            $('#img-upload-remove').remove();
        });

        var img_upload_thumb = $('#img-upload-thumb');

        $("#thumb").change(function () {
            readURL(this, img_upload_thumb);
            $('#img-upload-remove').remove();
        });


    },

    initdistricts: function () {
        $('#province_id').on('change', function (e) {
            e.preventDefault();
            var province_id = e.target.value;

            var url = $('.districts_url').val();
            var data = {
                province: province_id,
                '_token': $('[name=_token]').val()
            };
            $.get(url, data, function (data) {
                var select_user_district = $('.user_district');
                select_user_district.empty();
                select_user_district.append('<option value="" selected disabled="true">--- Quận Huyện ---</option>');

                $.each(data.response.data, function (index, data_dis) {
                    select_user_district.append('<option value="' + data_dis.id + '" >' + data_dis.district_name + '</option>');
                })
            });
        });

    },

    ininchecknumber: function () {
        var el = document.getElementById("phone");

        //---------------------------------Chỉ cho nhập số---------------------------------
        if (el) {
            el.addEventListener("keypress", function (evt) {
                if (
                    (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
                    evt.which > 57
                ) {
                    evt.preventDefault();
                }
            });
        }

        $(".nav-tabs a").click(function () {
            $(this).tab('show');
            lava.redrawCharts();
        });
    },

    initCheckbox: function () {
        $('#select_all').change(function () {
            var checkboxes = $(this).closest('table').find(':checkbox');

            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
                $('.table tbody tr').addClass('selected_item');
            } else {
                checkboxes.prop('checked', false);
                $('.table tbody tr').removeClass('selected_item');
            }
        });

        $('.checkbox_item').change(function () {
            var parent = $(this).parent().parent().parent().parent();

            if ($(this).is(':checked')) {
                parent.addClass('selected_item');
            } else {
                parent.removeClass('selected_item');
                $('#select_all').prop('checked', false);

            }

            var length_checkbox_item = $('.checkbox_item').length;
            var length_selected_item = $('.selected_item').length;
            if (length_checkbox_item == length_selected_item) {
                $('#select_all').prop('checked', true);
            }

        });

    },

    initBulkActionChange: function () {
        // update status list checkbox
        $(document).on('click', '.bulk_action_change', function () {
            var array = [];

            $("input.checkbox_item:checkbox[name=checkbox]:checked").each(function () {
                array.push($(this).val());
            });

            var token = $('#data-token').val();
            var id = array.join();
            var value = $(this).attr('value');
            var dataLink = $(this).attr('data-link');
            var link = $('#' + dataLink).val();
            var key = $(this).attr('data-key');
            var text = $(this).attr('data-text');
            var title = $(this).attr('data-title');

            if (id && link && key) {
                swal({
                    title: "Bạn có chắc chắn " + text + " " + title + " này ?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Yes",
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: "No"
                }, function () {
                    $('body').loading('toggle');

                    $.ajax({
                        type: 'PUT',
                        url: link,
                        data: {
                            "_token": token,
                            "key": key,
                            "value": value,
                            "ids": id
                        },
                        success: function (response) {
                            $('body').loading('toggle');
                        }
                    }).success(function (response) {
                        if (response.meta.code === 200) {
                            swal("Thành công");
                            location.reload();
                        } else {
                            swal({
                                type: "error",
                                title: response.meta.message + "!",
                                confirmButtonClass: "btn-danger",
                                html: true
                            });
                        }
                    });
                });

                return false;
            } else {
                swal({
                    type: "error",
                    title: "Chọn ít nhất một " + title + "!",
                    confirmButtonClass: "btn-danger",
                    html: true
                });
            }
        });
    },

    initItemActions: function () {
        // update status list checkbox
        $(document).on('click', '.item_actions', function () {
            var token = $('#data-token').val();
            var value = $(this).attr('data-val');
            var dataLink = $(this).attr('data-link');
            var link = $('#' + dataLink).val();
            var key = $(this).attr('data-key');
            var text = $(this).attr('data-text');
            var title = $(this).attr('data-title');
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');

            if (id && link && key) {
                swal({
                    title: "Bạn có chắc chắn " + text + " " + title + " này ?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: "Yes",
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: "No",
                }, function () {
                    $('body').loading('toggle');

                    $.ajax({
                        type: 'PUT',
                        url: link,
                        data: {
                            "_token": token,
                            "key": key,
                            "value": value,
                            "id": id,
                            "url": url,
                        },
                        success: function (response) {
                            $('body').loading('toggle');
                        }
                    }).success(function (response) {
                        if (response.meta.code === 200) {
                            swal("Thành công");
                            location.reload();
                        } else if (response.meta.code === 1) {
                            swal({
                                type: "error",
                                title: response.meta.message + "!",
                                confirmButtonClass: "btn-danger",
                                html: true
                            });

                            location.reload();
                        } else {
                            swal({
                                type: "error",
                                title: response.meta.message + "!",
                                confirmButtonClass: "btn-danger",
                                html: true
                            });
                        }
                    });
                });
                return false;
            }
        });
    },

    initPerPage: function () {
        // limit and order
        $(document).on('change', '#limit_pagination', function () {
            $('#form_custom_list').submit();
        });
    },

    initFilter: function () {
        // $(document).on('change', '.filter_header', function () {
        //     $('#form_custom_list').submit();
        // });
        var el = document.getElementById("mobile");

        //---------------------------------Chỉ cho nhập số---------------------------------
        if (el) {
            el.addEventListener("keypress", function (evt) {
                if (
                    (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
                    evt.which > 57
                ) {
                    evt.preventDefault();
                }
            });
        }

    },

    initActive: function () {
        $(".nav li").on("click", function () {
            $(".nav li").removeClass("active");
            $(this).addClass("active");
        });
    },

    initFilterDate: function () {

        $(".expand").on("click", function () {
            $(this).next().slideToggle(200);
            $expand = $(this).find(">:first-child");

            if ($expand.text() == "+") {
                $expand.text("-");
            } else {
                $expand.text("+");
            }
        });

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();
            if (startDate && endDate) {
                $('#reservation').val(startDate + ' - ' + endDate);
            } else {
                $('#reservation').val('');
            }

        }

        $('#reservation').daterangepicker({
            startDate: start,
            endDate: end,
            minDate: moment().subtract(2, 'years'),
            maxDate: moment(),
            dateLimit: {days: 730},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Tháng này': [moment().startOf('month'), moment()],
                'Tháng trước': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                '6 tháng trước': [moment().subtract(6, 'months'), moment()],
                'Năm nay': [moment().startOf('year'), moment()],
                'Năm trước': [moment().subtract(1, 'years').startOf('year'), moment().subtract(1, 'years').endOf('year')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'Từ',
                toLabel: 'Đến',
                format: 'DD/MM/YYYY',
                customRangeLabel: 'Tùy chỉnh',
                // daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                daysOfWeek: ['Cn', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                firstDay: 1
            }
        }, cb);
        cb(start, end);

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language: 'vn',
            dates: {
                days: ["Chủ nhật", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Tháng 1", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                today: "Today",
                clear: "Clear",
                titleFormat: "MM yyyy"
            }
        });

        function cbClass(start, end) {
            var startDate = $('.startDate').val();
            var endDate = $('.endDate').val();
            if (startDate && endDate) {
                $('.reservation').val(startDate + ' - ' + endDate);
            } else {
                $('.reservation').val('');
            }

        }

        $('.reservation').daterangepicker({
            startDate: start,
            endDate: end,
            // minDate: moment().subtract(2, 'years'),
            // maxDate: moment(),
            dateLimit: {days: 730},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Tháng này': [moment().startOf('month'), moment()],
                'Tháng trước': [moment().subtract(1, 'months').startOf('month'), moment().subtract(1, 'months').endOf('month')],
                '6 tháng trước': [moment().subtract(6, 'months'), moment()],
                'Năm nay': [moment().startOf('year'), moment()],
                'Năm trước': [moment().subtract(1, 'years').startOf('year'), moment().subtract(1, 'years').endOf('year')]
            },
            opens: 'right',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'Từ',
                toLabel: 'Đến',
                format: 'DD/MM/YYYY',
                customRangeLabel: 'Tùy chỉnh',
                // daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                daysOfWeek: ['Cn', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                firstDay: 1
            }
        }, cbClass);
        cbClass(start, end);
    },

};

Main.fn.initdatetimepicker();
Main.fn.initCheckbox();
Main.fn.initItemActions();
Main.fn.initBulkActionChange();
Main.fn.ininchecknumber();
Main.fn.initPerPage();
Main.fn.initFilter();
Main.fn.initFilterDate();
Main.fn.initActive();
Main.fn.initimage();
Main.fn.initdistricts();
