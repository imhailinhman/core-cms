@extends('frontend.layouts.master')
@section('content')

<!-- tour hour -->
<div class="container n3-tour-hour mt-30">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="title mg-bot10 animated" data-wow-delay="100ms" data-wow-iteration="infinite" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 300ms; animation-iteration-count: infinite; animation-name: pulse;">
                @if (!empty($products->first()))
                    <a href="{{ $products->first()->category->title_seo }}">Tour khuyến mại</a>
                @endif
            </h2>
        </div>
        <div id="TourDiscount">
            @if (!empty($products))
                @foreach($products->where('type', \App\Repositories\Interfaces\ProductRepository::TYPE_DISCOUNT)->shuffle()->take(3) as $key => $product)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                        <a href="{{ $product->category->title_seo."/".$product->title_seo  }}" title="{{ $product->title_seo  }}">
                            <div class="pos-relative">
                                <img src="{{ $product->image  }}" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải ">
                                <div class="frame-tgc1">
                                    <div class="row">
                                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                            <div class="f-left">
                                                <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                            </div>
                                            <div class="f-left date"><span class="yellow">{{ date('d/m/Y', strtotime($product->date_start)) }}</span> - <span class="yellow">{{ $product->sum_time }}</span></div>
                                            <div class="clear"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="frame-tgc2">
                                <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">{!! str_limit($product->name, $limit = 70, $end = '...') !!}</div>
                                <div class="tgc-line"></div>
                                <div class="mg-bot10">
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left tgc-info">
                                        @if ($product->price > 0 && $product->price_sale > 0)
                                            <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="price-o">{{ number_format($product->price_sale, 0, '', ',') }} đ</span>
                                        @elseif($product->price > 0 && $product->price_sale == 0)
                                            <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                        @elseif($product->price == 0)
                                            Giá từ : Đang cập nhật
                                        @endif
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

<div class="container n3-tour-hour">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="title mg-bot10">
                @if (!empty($products->first()))
                    <a href="{{ $products->first()->category->title_seo }}">Điểm đến hấp dẫn</a>
                @endif
            </h2>
        </div>
        <div id="TourHot">
            @if (!empty($products))
                @foreach($products->where('type', \App\Repositories\Interfaces\ProductRepository::TYPE_HOT)->shuffle()->take(3) as $key => $product)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mg-bot30 wow animated rollIn">
                        <a href="{{ $product->category->title_seo."/".$product->title_seo  }}" title="{{ $product->title_seo  }}">
                            <div class="pos-relative">
                                <img src="{{ $product->image  }}" class="img-responsive pic-tgc" alt="Nha Trang - Hòn Lao - Thế Giới Giải ">
                                <div class="frame-tgc1">
                                    <div class="row">
                                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                            <div class="f-left">
                                                <img src="{{asset('')}}layouts/images/i-date-w.png" alt="date">
                                            </div>
                                            <div class="f-left date"><span class="yellow">{{ date('d/m/Y', strtotime($product->date_start)) }}</span> - <span class="yellow">{{ $product->sum_time }}</span></div>
                                            <div class="clear"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="frame-tgc2">
                                <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">{!! str_limit($product->name, $limit = 70, $end = '...') !!}</div>
                                <div class="tgc-line"></div>
                                <div class="mg-bot10">
                                    <img src="{{asset('')}}layouts/images/i-price.png" class="f-left" alt="price">
                                    <div class="f-left tgc-info">
                                        @if ($product->price > 0 && $product->price_sale > 0)
                                            <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="price-o">{{ number_format($product->price_sale, 0, '', ',') }} đ</span>
                                        @elseif($product->price > 0 && $product->price_sale == 0)
                                            <span class="price-n">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                        @elseif($product->price == 0)
                                            Giá từ : Đang cập nhật
                                        @endif
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

<section id="br-section">
    <div class="container">
        <div class="col-sm-8 col-sm-push-2">
            <div class="br-text">
                <h2 style="">
                    <strong>Du lịch </strong>cho cuộc sống
                    <span class="subtitle" style="color:#ffffff">Hưởng niềm vui chọn vẹn</span>
                </h2>
            </div>
        </div>
    </div>
</section>

<!-- destination -->
<div class="container n3-destination mt-30">
    <div class="row">
        <div class="col-xs-12 mg-bot30">
            <h2 class="title mg-bot10"><a>Điểm đến yêu thích</a></h2>
        </div>
        @if (!empty($products))
            @foreach($products->take(4) as $key => $product)
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot30 wow animated rollIn" style="animation-delay: 0.6s;">
                    <div class="pos-relative">
                        <a href="{{ $product->category->title_seo."/".$product->title_seo  }}" class="hvr-bounce-to-bottom">
                            <img src="{{ $product->image }}" alt="{{ !empty($product->provider->province_name) ? $product->provider->province_name : '' }}" class="img-responsive dd-img">
                            <div class="frame-nation">
                                <div>
                                    <div class="nation-name"><span class="imp">{{ !empty($product->endPoint->province_name) ? $product->endPoint->province_name : '' }}</span></div>
                                    <div class="nation-expl f-left">Khám phá ngay</div>
                                    <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        @endif

        <div class="col-xs-12">
            <div style="border-top: 1px dashed #ccc;padding-bottom: 30px;padding-top: 0px;"></div>
        </div>
        <div class="nation">
            @if (!empty($products))
                @foreach($products->slice(4)->take(5) as $key => $product)
                    <div class="col-lg-2 col-xs-12 mg-bot30 wow animated rollIn">
                        <a href="{{ !empty($product->category->title_seo) && !empty($product->title_seo) ? $product->category->title_seo."/".$product->title_seo : ''  }}" class="hvr-sweep-to-right" title="Ch&#226;u &#194;u">
                            <div class="pos-relative">
                                <img src="{{ $product->image }}" alt="Ch&#226;u &#194;u" class="img-responsive qg-img">
                                <div class="frame-nation">
                                    <div class="nation-name"><span class="imp">{{ !empty($product->endPoint->province_name) ? $product->endPoint->province_name : '' }}</span></div>
                                    <div>
                                        <div class="nation-expl f-left">Khám phá ngay</div>
                                        <div class="f-left"><img src="{{asset('')}}layouts/images/i-arrow.png" alt="arrow" class="img-responsive"></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>



<!--NEW-->
<div class="container n3-tour-hour">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="title mg-bot10">
                <a href="{{ route('new.index') }}">Tin tức</a>
            </h2>
        </div>
        <div id="NewTour">
            @if (!empty($news))
                @foreach($news as $key => $new)
                    @php
                        $route = route('newInfo.index', ['id' => $new->slug]);
                        if($new->type === \App\Repositories\Interfaces\NewRepository::TYPE_PROMOTION) {
                            $route = route('promotionInfo.index', ['id' => $new->slug]);
                        }
                    @endphp

                    <div class="col-md-3 col-md-3 col-sm-6 col-xs-12 mg-bot30">
                        <a href="{{ $route }}" title="{{ $new->title }}">
                            <div class="pos-relative">
                                <img src="{{ $new->image }}" class="img-responsive pic-tgc" alt="{{ $new->title }}">
                            </div>
                            <div class="frame-tgc2">
                                <div class="tgc-title dot-dot-ajax cut-tgc ddd-truncated" style="overflow-wrap: break-word;">{!! str_limit($new->title, $limit = 30, $end = '...') !!}</div>
                                <div class="tgc-line"></div>
                                <div class="mg-bot10 text-center">
                                    <div class="clear"></div>
                                    <div class="text-right">
                                        <a href="{{ $route }}" class="view_more text-red" title="Có gì ở lễ hội hoa Floriade lớn nhất Nam bán cầu?">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

@endsection
