@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- breadcrumb -->
    <div class="n3-breadcrumb mt-30">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="breadcrumb">
                        <span><a href="" itemprop="url"><span itemprop="title">Visa</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="visa">
        <div class="container n3-contact mg-bot40 mt-30">
            <div class="row">
                <div class="col-xs-12 mg-bot15">
                    <div class="title mg-bot15"><h1>Tư vấn visa</h1></div>
                </div>
                <div class="col-xs-12 mg-bot50 visa-body">
                    @if (!empty($visas))
                        @foreach($visas as $key => $visa)
                            <div class="col-sm-4 col-xs-12 visa-body-all mt-15">
                                <a href="{{ route('visaInfo.index', ['id' => $visa->slug]) }}">
                                    <span class="visa-img">
                                        <img src="{{ $visa->image }}" alt="{{ $visa->title }}" class="w-100">
                                    </span>
                                </a>
                                <a href="{{ route('visaInfo.index', ['id' => $visa->slug]) }}">
                                    <h3 class="visa-title">{{ $visa->title }}</h3>
                                </a>

                                <span class="visa-span">  {!! str_limit($visa->description, $limit = 70, $end = '...') !!}</span><br>

                                <a href="{{ route('visaInfo.index', ['id' => $visa->slug]) }}" class="view_more" title="">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>


</div>
@endsection
