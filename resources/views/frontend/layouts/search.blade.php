@inject('searchHelper', 'App\Helpers\SearchHelper')

<div class="n3-form-search" style="z-index: initial; position: initial; background: rgb(241, 241, 241);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="search-box">
                    <form action="{{ route('search.index') }}" method="get" role="form">
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-12 col-md-4">
                                <input id="search_key" class="form-control search_name enter-submit" name="search_key" placeholder="Từ khóa tìm kiếm..." value="{{ request('search_key') }}"/>
                            </div>

                            <div class="form-group col-xs-12 col-sm-12 col-md-3">
                                <select name="category" id="category" class="form-control select2">
                                    <option value="">Tất cả tour</option>
                                    @if(!empty($searchHelper->ListCategory()))
                                        @foreach($searchHelper->ListCategory() as $key => $search)
                                            <option value="{{ $key }}" {!! (request('category') == $key) ? 'selected="selected"' : '' !!}>{{ $search }}</option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-3">
                                <select name="province" id="province" class="form-control select2">
                                    <option value="">Tất cả địa điểm</option>
                                    @if(!empty($searchHelper->ListProvince()))
                                        @foreach($searchHelper->ListProvince() as $key => $search)
                                            <option value="{{ $key }}" {!! (request('province') == $key) ? 'selected="selected"' : '' !!}>{{ $search }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-2 text-center">
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('js_frontend')
    <script type="text/javascript">
    $(document).ready(function () {
        $('#category').select2({
            // placeholder: 'Chọn Tour'
        });

        $('#province').select2({
            // placeholder: 'Chọn địa điểm'
        });
    });
    </script>
@endsection