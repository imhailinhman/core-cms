{!! SEOMeta::generate() !!}
{!! OpenGraph::generate() !!}
{!! Twitter::generate() !!}
{!! SEO::generate(true) !!}
<meta name="apple-itunes-app" content="app-id=1444566830, app-argument=spaceships://spaceship/7" />
{{--<link rel="icon" href="{{asset('')}}user/img/icon.png?v=1" sizes="32x32">--}}
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148415775-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-148415775-1');
</script>