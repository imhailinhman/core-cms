@extends('frontend.layouts.master')
@section('content')
<div class="container">

    <!-- new-bar -->
    <div class="container n3-list-tour mg-bot40">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="list-tour">
                    <div class="list">
                        <div class="row">
                            <!-- ToursHot -->
                            <div class="col-md-12 text-center list-tour-title">
                                <h2 class="title">
                                    <a href="#">Danh sách tìm kiếm</a>
                                </h2>
                            </div>

                            <!--list tour-->
                            @if (!empty($products))
                                @foreach($products as $key => $product)
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="item mg-bot30">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="tour-name">
                                                        <a href="{{ $product->category->slug.'/'.$product->title_seo }}" title="{{ $product->name }}"><h3>{{ $product->name }}</h3></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="pos-relative">
                                                        <a href="{{ $product->title_seo }}" title="{{ $product->name }}"><img src="{{ $product->image }}" class="img-responsive pic-lt" alt="{{ $product->name }}"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                    <div class="frame-info">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-code.png" alt="code"></div>
                                                                <div class="f-left r">{{ $product->code_tour }}</div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-chair.png" alt="chair"></div>
                                                                <div class="f-left r">
                                                                    Số chỗ: <span class="font500">{{ $product->seat }}</span>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                                <div class="f-left r">Ngày đi: <span class="font500">{{ date('d/m/Y', strtotime($product->date_start)) }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-clock.png" alt="clock"></div>
                                                                <div class="f-left r">Ngày về: <span class="font500">{{ date('d/m/Y', strtotime($product->date_end)) }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 mg-bot10">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-price.png" alt="price"></div>
                                                                <div class="f-left r">
                                                                    Giá: <span class="font500 price">{{ number_format($product->price, 0, '', ',') }} đ</span>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="f-left l"><img src="{{asset('')}}layouts/images/i-date.png" alt="date"></div>
                                                                <div class="f-left r">Số ngày: <span class="font500">{{ $product->sum_time }}</span></div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div id="divEndTour"></div>
                        </div>
                    </div>

                        <div class="col-xs-12 text-right mg-bot30">
                            <div class="pager_simple_orange text-center">
                                @if(isset($products) && !empty($products))
                                    {!! $products->appends(request()->query())->links() !!}
                                @endif
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
