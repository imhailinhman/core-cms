@extends('frontend.layouts.master')
@section('css_frontend')
    <style type="text/css" media="screen">
        #messages{
            color: #1abc9c;
        }
        #messages li{
            max-width: 50%;
            margin-bottom:10px;
            border-color: #34495e;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="container">
            <div class="content">
                <h1>Laravel & Pusher: Demo real-time web application.</h1>
                <small>
                    Author: <a href="https://trungquandev.com/" target="__blank">https://trungquandev.com/</a>
                </small><br><br>
                <p>Message preview:</p>
                <ul id="messages" class="list-group"></ul>
            </div>
        </div>
    </div>
@endsection

@section('js_frontend')

<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>
    $(document).ready(function(){
        // Khởi tạo một đối tượng Pusher với app_key
        var pusher = new Pusher('64f5fd6c2554fcab82b6', {
            cluster: 'ap1',
            encrypted: true
        });
        //Đăng ký với kênh chanel-real-time mà ta đã tạo trong file ContactPusherEvent.php
        var channel = pusher.subscribe('channel-real-time');
        //Bind một function addMesagePusher với sự kiện DemoPusherEvent
        channel.bind('App\\Events\\ContactPusherEvent', addMessageDemo);
    });
    //function add message
    function addMessageDemo(data) {
        var liTag = $("<li class='list-group-item'></li>");
        liTag.html(data.message);
        $('#messages').append(liTag);
    }
</script>

@endsection
