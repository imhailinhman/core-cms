@extends('frontend.layouts.master')
@section('content')
    <div class="container">

        <!-- breadcrumb -->
        <div class="n3-breadcrumb mt-30">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="breadcrumb">
                            <span><a href="" itemprop="url"><span itemprop="title">Liên hệ </span></a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container n3-contact mg-bot40">
            <div class="row">
                <div class="col-xs-12 mg-bot15">
                    <div class="title mg-bot15"><h1>Liên hệ</h1></div>
                    <div class="text-center">Để có thể đáp ứng được các yêu cầu và các ý kiến đóng góp của quý vị một cách nhanh chóng xin vui lòng liên hệ</div>
                </div>
                <div class="col-xs-12 mg-bot50">
                    <form action="" id="myform" method="post" class="form" enctype="multipart/form-data"  onsubmit="submitContact(this); return false;">
                        @csrf
                        {{--{{ csrf_field() }}--}}
                        <div class="frame-contact">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                    <label>Họ tên (<span class="star">*</span>)</label>
                                    <input type="text" class="form-control" required="required" name="name" id="name">
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                    <label>Email (<span class="star">*</span>)</label>
                                    <input type="email" class="form-control" name="email" id="email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mg-bot15">
                                    <label>Điện thoại (<span class="star">*</span>)</label>
                                    <input type="number" class="form-control" name="phone" id="phone" required="required">
                                </div>

                                <div class="col-xs-12 mg-bot15">
                                    <label>Địa chỉ</label>
                                    <input type="text" class="form-control" name="address">
                                </div>

                                <div class="col-xs-12 mg-bot15">
                                    <label>Tiêu đề (<span class="star">*</span>)</label>
                                    <input type="text" class="form-control" name="note">
                                </div>
                                <div class="col-xs-12 mg-bot30">
                                    <label>Nội dung (<span class="star">*</span>)</label>
                                    <textarea class="form-control" rows="4" cols="5" name="comment"></textarea>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-md btn-general">Gửi đi &nbsp;&nbsp;<i class="fas fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js_frontend')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function submitContact() {
        $.ajax({
            url: '{{ route('lienhe.create') }}',
            dataType: 'json',
            type: 'post',
            data: $('#myform').serialize(),
            cache: false,
            success: function (res) {
                $("form").trigger("reset");
                if (res.meta.success) {
                    successNotify(res.meta.message);
                } else {
                    errorNotify(res.meta.message);
                }
            }, error: function (res) {
                errorNotify(res);
            }
        });
    }
</script>
@endsection
