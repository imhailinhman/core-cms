@extends('Backend::layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="text-danger">Bạn không có quyền thực hiện chức năng này.</h1>
            </div>
        </div>
    </div>
@endsection
