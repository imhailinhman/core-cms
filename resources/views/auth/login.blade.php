<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>Gutina CMS</title>
    @include('backend.layouts.datatables_css')

</head>
<body class="hold-transition login-page">
<div class="login-box">
    {{--<div class="login-logo">--}}
        {{--<a href="{{ url('/home') }}"><img src="/img/gutina.png"/></a>--}}
    {{--</div>--}}

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">ĐĂNG NHẬP HỆ THỐNG</p>

        @include('adminlte-templates::common.errors')
        {!! Form::open([url('/login'), 'class' => 'form jsvalidation', 'novalidate' => 'novalidate']) !!}

            {!! csrf_field() !!}

            <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                <input type="username" class="form-control" name="username" value="{{ old('username') }}" placeholder="Tài khoản">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Mật khẩu" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>

        {!! Form::close() !!}

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@include('backend.layouts.datatables_js')
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
