@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('slides.index') !!}">Quản trị</a></li><li class="active">Tạo mới slide</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Thêm mới slide
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'slides.store', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true, 'enctype' => 'multipart/form-data']) !!}

                    @include('backend.slides.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
