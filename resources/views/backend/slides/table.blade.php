<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="example">
    <thead>
    <tr>
        <th class="text-left nosort">
            <label class="mb-0">
                <input type="checkbox" value="" id="select_all" name="select_all">
            </label>
        </th>
        <th class="text-left nosort">Stt</th>
        <th class="text-left nosort">Ảnh</th>
        <th class="text-left nosort">Name</th>
        <th class="text-left nosort">Tin tức</th>
        <th class="text-left nosort">Trạng thái</th>
        <th class="text-left nosort">Hành động</th>
    </tr>
    </thead>
    <tbody class="break-word">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($slides as $k => $slide)
        @php
            $count_number++;
        @endphp
        <tr id="tr-{!! $slide->id !!}">
            <td class="text-left p-t-0">
                <div class="checkbox m-t-5">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$slide->id}}">
                    </label>
                </div>
            </td>
            <td align="left">{{ $count_number }}</td>
            <td align="left">
                <a target="_blank" href="{{ !empty($slide->image) ? $slide->image : '' }}">
                    <img src="{{ !empty($slide->image) ? $slide->image : '' }}" class="  img-fluid img-thumbnail center-block " width="100px" height="100px">
                </a>
            </td>
            <td align="left">{{ !empty($slide->title) ? $slide->title : '' }}</td>
            <td align="left">{{ !empty($slide->new) ? $slide->new->title : '' }}</td>
            <td class="text-left">
                @if($slide->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="slide" data-text="vô hiệu" data-val="0"
                            data-id="{!! $slide->id !!}">Kích hoạt
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="slide" data-text="kích hoạt" data-val="1"
                            data-id="{!! $slide->id !!}">Vô hiệu
                    </button>
                @endif
            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="{!! $slide->link !!}" class='btn btn-default btn-xs' target="_blank">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="{!! route('slides.edit', [$slide->id]) !!}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="slide" data-text="Xóa" data-val="none" data-id="{!! $slide->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($slides) && !empty($slides))
        {!! $slides->appends(request()->query())->links() !!}
    @endif
</div>
