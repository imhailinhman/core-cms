
<div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
    <img id="img-upload" class="img-fluid center-block" width="200px" height="200px"/>
    <div id="img-upload-remove">
        @if (isset($model->image))
            <img src="{{$model->image }}" class=" img-fluid img-thumbnail center-block "
                 width="200px" height="200px">
            <input type="hidden" name="image_tmp" value="{{$model->image}}"/>
        @else
            <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                 width="200px" height="200px">

        @endif
    </div>
    <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
        {!! Html::decode(Form::label('image','Ảnh <span class="required_data">*</span>',['class' => 'font-weight-bold float-left mr-4'])) !!}
        {!! Form::file('image', ['class' => 'form-control w-75' ,'accept' => 'image/*' , 'name' => 'image' ,'placeholder' => 'Ảnh' , 'id' => 'image' ]) !!}
    </div>

</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Tiêu đề:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('title', (!empty($model)) ? $model->title : old('title'), ['placeholder'=>'Tiêu đề', 'class' => 'form-control']) !!}
</div>

<!-- company_id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Danh mục cha:') !!}
    <span class="text-danger">(*)</span>
    <select name="category_id" id="category_id" class="form-control select2">
        <option value="">-------- Danh mục --------</option>
        @if(!empty($categores))
            @foreach($categores as $k_id => $category)
                <?php
                    if (!empty($model)) {
                        $selected = ($k_id == $model->category_id) ? 'selected' : null;
                    } else {
                        $selected = '';
                    }
                ?>
                <option {{$selected}} value="{{ $k_id }}" {{ (old("category_id") == $k_id ? "selected":"") }}>{{ $category }}</option>
            @endforeach
        @endif
    </select>
</div>

<!-- Tags Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('tags', 'Tags:') !!}
    <div class="clearfix"></div>
    {!! Form::text('tags', (!empty($model)) ? $model->tags : old('tags'), ['class' => 'form-control', 'data-role' => 'tagsinput', 'id' => 'tags-input']) !!}
</div>

{{--<!-- Keyword Seo Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('date_up', 'Thời gian đăng bài:') !!}--}}
    {{--<div class='input-group date' id='datetimepicker' data-date-format='dd-mm-yyyy'>--}}
        {{--<input name="date_up" type='text' class="form-control"--}}
               {{--value="{!! (!empty($model) && $model->date_up) ? Helper::formatDate($model->date_up, 'd-m-Y') : old('date_up') !!}" required/>--}}
        {{--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>--}}
    {{--</div>--}}
{{--</div>--}}

<!-- Keyword Seo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keyword_seo', 'keyword_seo:') !!}
    {!! Form::text('keyword_seo', (!empty($model)) ? $model->keyword_seo : old('keyword_seo'), ['placeholder'=>'Keyword Seo', 'class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('type', 'Kiểu tin tức:') !!}
    {!! Form::select('type', \App\Models\News::typeNew(), (!empty($model)) ? $model->type : old('type') , ['class' => 'form-control']) !!}
</div>


<!-- Description Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('description', 'Tóm tắt:') !!}
    {!! Form::textarea('description', (!empty($model)) ? $model->description : old('description'), ['placeholder'=>'Tóm tắt', 'class' => 'form-control', 'rows' => 2]) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Mô tả :') !!}
    {!! Form::textarea('content', (!empty($model)) ? $model->content : old('content'), ['placeholder'=>'Keyword Seo', 'class' => 'form-control', 'id' => 'editor1' ]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12 col-xs-12 col-md-12">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('news.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#category_id').select2({
            placeholder: '---Tất cả---'
        });

        $('#formid').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(function () {
            CKEDITOR.replace('editor1',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        })
    </script>
@endsection
