@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="clearfix"></div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->
            <input id="data-link-bulk-actions" value="{{ route("categories.bulkActions") }}" hidden>
            <input id="data-link-item-actions" value="{{ route("categories.itemActions") }}" hidden>

            <form action="" method="get" id="form_custom_list">
                @include('backend.categories.filter')
                <div class="div_option form-group mg-top-10">
                    <div class="col-xs-3 col-md-6 col-sm-6">
                        <ul class="nav navbar-left panel_toolbox">
                            <li class="dropdown">
                                <select class="form-control" name="limit" id="limit_pagination">
                                    <option @if (request('limit') == 10)selected="selected" @endif value="10">10</option>
                                    <option @if (request('limit') == 25)selected="selected" @endif value="25">25</option>
                                    <option @if (request('limit') == 50)selected="selected" @endif value="50">50</option>
                                    <option @if (request('limit') == 75)selected="selected" @endif value="75">75</option>
                                    <option @if (request('limit') == 100)selected="selected" @endif value="100">100</option>
                                </select>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="{{ route("categories.create") }}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới Danh mục
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Danh sách danh mục </h3>
                </div>

                <div class="box-body">
                    @include('flash::message')
                    @include('backend.categories.table')
                </div>
            </div>
        </div>

    </div>
@endsection

