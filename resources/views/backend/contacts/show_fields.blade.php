<!-- Profile Image -->

    <div class="box-body box-profile">
        <!-- Name Field -->

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            <p>{!! $contact->name !!}</p>
        </div>

        <!-- Email Field -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            <p>{!! $contact->email !!}</p>
        </div>

        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', 'Số điện thoại:') !!}
            <p>{!! $contact->phone !!}</p>
        </div>

        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('address', 'Địa chỉ:') !!}
            <p>{!! $contact->address !!}</p>
        </div>

        <!-- Note Field -->
        <div class="form-group">
            {!! Form::label('note', 'Tiêu đề:') !!}
            <p>{!! $contact->note !!}</p>
        </div>

        <!-- Comment Field -->
        <div class="form-group">
            {!! Form::label('comment', 'Nội dung:') !!}
            <p>{!! $contact->comment !!}</p>
        </div>
    </div>
