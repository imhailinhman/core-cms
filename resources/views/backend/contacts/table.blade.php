<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="contacts">
    <thead>
    <tr>
        <th class="text-left nosort">
            <label class="mb-0">
                <input type="checkbox" value="" id="select_all" name="select_all">
            </label>
        </th>
        <th class="text-left nosort">Stt</th>
        <th class="text-left nosort">Tên</th>
        <th class="text-left nosort">Email</th>
        <th class="text-left nosort">Số điện thoại</th>
        <th class="text-left nosort">Địa chỉ</th>
        <th class="text-left nosort">Visa</th>
        <th class="text-left nosort">Trạng thái</th>
        <th class="text-left nosort">Hành động</th>
    </tr>
    </thead>
    <tbody class="break-word contacts">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($contacts as $k => $contact)
        @php
            $count_number++;
        @endphp
        <tr id="tr-{!! $contact->id !!}">
            <td class="text-left p-t-0">
                <div class="checkbox m-t-5">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$contact->id}}">
                    </label>
                </div>
            </td>
            <td align="left">{!! $count_number !!}</td>
            <td align="left">{!! $contact->name !!}</td>
            <td align="left">{!! $contact->email !!}</td>
            <td align="left">{!! $contact->phone !!}</td>
            <td align="left">{!! $contact->address !!}</td>
            <td align="left">{!! !empty($contact->visa) ? $contact->visa->title : ''; !!}</td>
            <td class="text-left">
                @if($contact->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions">Đã xem</button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions">Chưa xem</button>
                @endif
            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="{!! route('contacts.show', [$contact->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="liên hệ" data-text="Xóa" data-val="none" data-id="{!! $contact->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($contacts) && !empty($contacts))
        {!! $contacts->appends(request()->query())->links() !!}
    @endif
</div>
