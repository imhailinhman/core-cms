@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('users.index') !!}">Danh sách khách hàng </a></li><li class="active">Chi tiết khách hàng {!! $user->fullname !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            {!! $user->fullname !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                @include('backend.users.show_fields')
                <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-xs-12 row text-center">
                        <a href="{!! route('users.index') !!}" class="btn btn-default">Quay lại</a>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
@endsection
