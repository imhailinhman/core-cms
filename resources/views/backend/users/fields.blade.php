
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
        <img id="img-upload" class="img-fluid center-block" width="200px" height="200px"/>
        @if(isset($model))
            <div id="img-upload-remove">
                @if (isset($model->avatar))
                    <img src="{{$model->avatar }}" class="  img-fluid img-thumbnail center-block "
                         width="200px" height="200px">
                    <input type="hidden" name="image_tmp" value="{{$model->avatar}}"/>
                @else
                    <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                         width="200px" height="200px">

                @endif
            </div>
        @endif

    </div>

    <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
        {!! Html::decode(Form::label('image','Ảnh <span class="required_data">*</span>',['class' => 'font-weight-bold float-left mr-4'])) !!}
        {!! Form::file('image', ['class' => 'form-control w-75' ,'accept' => 'image/*' , 'name' => 'image' ,'placeholder' => 'Ảnh' , 'id' => 'image' ]) !!}
    </div>

    <!-- fullname Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('fullname', 'Tên đầy đủ :') !!}
        {!! Form::text('fullname', (!empty($model)) ? $model->fullname : old('fullname'), ['placeholder'=>'Tên đầy đủ ', 'class' => 'form-control']) !!}
    </div>

    <!-- fullname Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('address', 'Giới tính :') !!}
        <span class="text-danger">(*)</span>
        {!!  Form::select('gender', ['' => '-------- Giới tính --------'] + ['0' => 'Nữ' , '1' => 'Nam', '2' => 'Giới tính khác'], (!empty($model)) ? $model->gender : old('gender') , ['class' => 'form-control']) !!}
    </div>

@if(\Route::current()->getName() === 'users.edit')
    <!-- Password Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('password', 'Mật khẩu :') !!}
            <input type="checkbox" name="changepassword" id="changepassword" title="">

            <input class="form-control password" name="password" placeholder="Change password"
                   id="up_admin_password" type="password" disabled="disabled">
        </div>

        <!-- Password Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('password', 'Nhập lại mật khẩu :') !!}
            <input class="form-control password" name="confirm_password" placeholder="confirm change password"
                   id="confirm_password" type="password" disabled="disabled">
        </div>
@else

    <!-- Password Field -->
        <div class="form-group col-sm-12 col-xs-12">
            {!! Form::label('password', 'Mật khẩu : ', array()) !!}
            <span class="text-danger">(*)</span>
            {{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control' ) ) }}
        </div>
@endif

<!-- Status Field -->
    <div class="form-group col-sm-12 col-xs-12 col-md-12">

        {!! Form::label('status', 'Trạng thái :') !!}
        <div class="material-switch float-left">
            {!! Form::hidden('status', false) !!}
            <?php
            $array_atr = ['id' => 'someSwitchOptionSuccess'];
            if (!empty($model) && $model != '') {
                if ($model->status == 1) {
                    $array_atr['checked'] = 'checked';
                }
            } else {
                $array_atr['checked'] = 'checked';
            }
            ?>
            {!! Form::checkbox('status', 1, null, $array_atr) !!}
            <label for="someSwitchOptionSuccess" class="label-success"></label>

        </div>

    </div>

</div>

<div class="col-md-6 col-sm-6 col-xs-12">

    <!-- Address Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('address', 'Địa chỉ :') !!}
        {!! Form::text('address', (!empty($model)) ? $model->address : old('address'), ['placeholder'=>'Địa chỉ', 'class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('email', 'Email :') !!}
        <span class="text-danger">(*)</span>
        {!! Form::text('email', (!empty($model)) ? $model->email : old('email'), ['placeholder'=>'Email', 'class' => 'form-control','id' => 'email', !empty($model->email)? 'readonly' : '' ]) !!}

    </div>

    <!-- Mobile Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('phone', 'Số điện thoại :') !!}
        <span class="text-danger">(*)</span>
        {!! Form::number('phone', (!empty($model)) ? $model->phone : old('phone'), ['placeholder'=>'Số điện thoại ', 'class' => 'form-control', !empty($model->phone)? 'readonly' : '' ]) !!}

    </div>

    {{--<!-- Ngày sinh Field -->--}}
    {{--<div class="form-group col-sm-12 col-xs-12">--}}
        {{--{!! Form::label('birthdate', 'Ngày sinh : ', array()) !!}--}}
        {{--<div class='input-group date' id='datetimepicker' data-date-format='yyyy-mm-dd'>--}}
            {{--<input name="birthdate" type='text' class="form-control"--}}
                   {{--value="{!! (!empty($model) && $model->birthdate) ? Helper::formatDate($model->birthdate) : old('birthdate') !!}"/>--}}
            {{--<span class="input-group-addon">--}}
                    {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                {{--</span>--}}
        {{--</div>--}}
    {{--</div>--}}

    <!-- Activated Field -->
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 col-xs-12 row text-center">
    <a href="{!! route('users.index') !!}" class="btn btn-default">Thoát</a>

    {!! Form::submit('Lưu', ['class' => 'btn btn-success']) !!}
</div>

<div class="clearfix"></div>

