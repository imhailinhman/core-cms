@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="clearfix"></div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->
            <input id="data-link-bulk-actions" value="{{ route("users.bulkActions") }}" hidden>
            <input id="data-link-item-actions" value="{{ route("users.itemActions") }}" hidden>

            <form action="" method="get" id="form_custom_list">
                @include('backend.users.filter')
                <div class="div_option form-group mg-top-10">

                    <div class="col-xs-3 col-md-6 col-sm-6">
                        <ul class="nav navbar-left panel_toolbox">
                            <li class="dropdown">
                                <select class="form-control" name="limit" id="limit_pagination">
                                    <option @if (request('limit') == 10)selected="selected" @endif value="10">10</option>
                                    <option @if (request('limit') == 25)selected="selected" @endif value="25">25</option>
                                    <option @if (request('limit') == 50)selected="selected" @endif value="50">50</option>
                                    <option @if (request('limit') == 75)selected="selected" @endif value="75">75</option>
                                    <option @if (request('limit') == 100)selected="selected" @endif value="100">100</option>
                                </select>
                            </li>
                        </ul>
                    </div>


                    <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                        <a href="{{ route("users.create") }}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới khách hàng
                        </a>
                        <a href="javascript:void(0);" value="none"
                           class="btn-toolbox btn btn-danger bulk_action_change"
                           data-link="data-link-bulk-actions" data-key="delete"
                           data-title="khách hàng" data-text="xóa tất cả"><i class="glyphicon glyphicon-trash"></i>Xóa tất cả
                        </a>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Danh sách khách hàng </h3>
                </div>

                <div class="box-body">
                    @include('flash::message')
                    @include('backend.users.table')
                </div>
            </div>
            <div class="text-center">

            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var pScroll = [];
        $(document).ready(function () {
            $('#from_date').on('keyup change paste', function(e) {
                var value = $( this ).val();
                $('#to_date').val(value);
            });

            $('#date_time').change(function(){
                var value = $(this).val();
                if (value == 1){
                    var startDate = moment().format('DD/MM/YYYY');
                    var endDate = moment().format('DD/MM/YYYY');
                }else if(value == 2){
                    var startDate = moment().subtract(1, 'days').format('DD/MM/YYYY');
                    var endDate = moment().subtract(1, 'days').format('DD/MM/YYYY');
                }else if(value == 3){
                    var startDate = moment().startOf('isoWeek').format('DD/MM/YYYY');
                    var endDate   = moment().endOf('isoWeek').format('DD/MM/YYYY');
                }else if(value == 4){
                    var startDate = moment().subtract(1, 'week').startOf('isoWeek').format('DD/MM/YYYY');
                    var endDate = moment().subtract(1, 'week').endOf('isoWeek').format('DD/MM/YYYY');
                }else if(value == 5){
                    var startDate = moment().startOf('month').format('DD/MM/YYYY');
                    var endDate = moment().endOf('month').format('DD/MM/YYYY');
                }else if(value == 6){
                    var startDate = moment().subtract(1, 'months').startOf('month').format('DD/MM/YYYY');
                    var endDate = moment().subtract(1, 'months').endOf('month').format('DD/MM/YYYY');
                }else if(value == 7){
                    var startDate = moment().startOf('years').format('DD/MM/YYYY');
                    var endDate = moment().endOf('years').format('DD/MM/YYYY');
                }else if(value == 8){
                    var startDate = moment().subtract(1, 'years').startOf('years').format('DD/MM/YYYY');
                    var endDate = moment().subtract(1, 'years').endOf('years').format('DD/MM/YYYY');
                }else if(value == 0){
                    var startDate = '';
                    var endDate = '';
                }

                $('#from_date').val(startDate);
                $('#to_date').val(endDate);
            });

        });
    </script>
@endsection
