<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="example">
    <thead>
    <tr>
        <th class="text-left nosort">
            <label class="mb-0">
                <input type="checkbox" value="" id="select_all" name="select_all">
            </label>
        </th>
        <th class="text-left">Stt</th>
        <th class="text-left">Tên đầy đủ</th>
        <th class="text-left">Số điện thoại</th>
        <th class="text-left">Địa chỉ</th>
        <th class="text-left">Trạng thái</th>
        <th class="text-left">Hành động </th>
    </tr>
    </thead>
    <tbody class="break-word">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($users as $user)
        @php $count_number++ @endphp
        <tr id="tr-{!! $user->id !!}">
            <td class="text-left p-t-0">
                <div class="checkbox m-t-5">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$user->id}}">
                    </label>
                </div>
            </td>
            <td align="left">{!! $count_number !!}</td>
            <td align="left">{!! $user->fullname !!}</td>
            <td align="left">{!! $user->phone !!}</td>
            <td align="left">{!! $user->address !!}</td>
            <td class="text-left">
                @if($user->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="khách hàng" data-text="ngừng hoạt động" data-val="0"
                            data-id="{!! $user->id !!}">Hoạt động
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="khách hàng" data-text=" hoạt động" data-val="1"
                            data-id="{!! $user->id !!}">Ngừng hoạt động
                    </button>
                @endif
            </td>

            <td align="left">
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="khách hàng" data-text="xóa"
                       data-val="none" data-id="{!! $user->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($users) && !empty($users))
        {!! $users->appends(request()->query())->links() !!}
    @endif

</div>
