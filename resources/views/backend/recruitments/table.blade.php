<table class="table table-responsive" id="recruitments-table">
    <thead>
        <tr>
            <th>Position</th>
        <th>Alias</th>
        <th>Quantity</th>
        <th>Type</th>
        <th>Experience</th>
        <th>Salary</th>
        <th>Diploma</th>
        <th>Description</th>
        <th>Benefit</th>
        <th>Requirement</th>
        <th>Profile</th>
        <th>Image</th>
        <th>Thumb</th>
        <th>Time Out</th>
        <th>Time</th>
        <th>Category Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($recruitments as $recruitment)
        <tr>
            <td>{!! $recruitment->position !!}</td>
            <td>{!! $recruitment->alias !!}</td>
            <td>{!! $recruitment->quantity !!}</td>
            <td>{!! $recruitment->type !!}</td>
            <td>{!! $recruitment->experience !!}</td>
            <td>{!! $recruitment->salary !!}</td>
            <td>{!! $recruitment->diploma !!}</td>
            <td>{!! $recruitment->description !!}</td>
            <td>{!! $recruitment->benefit !!}</td>
            <td>{!! $recruitment->requirement !!}</td>
            <td>{!! $recruitment->profile !!}</td>
            <td>{!! $recruitment->image !!}</td>
            <td>{!! $recruitment->thumb !!}</td>
            <td>{!! $recruitment->time_out !!}</td>
            <td>{!! $recruitment->time !!}</td>
            <td>{!! $recruitment->category_id !!}</td>
            <td>{!! $recruitment->status !!}</td>
            <td>
                {!! Form::open(['route' => ['recruitments.destroy', $recruitment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('recruitments.show', [$recruitment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('recruitments.edit', [$recruitment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>