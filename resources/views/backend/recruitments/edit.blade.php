@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Recruitment
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($recruitment, ['route' => ['recruitments.update', $recruitment->id], 'method' => 'patch']) !!}

                        @include('recruitments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection