@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="clearfix"></div>

            <input id="data-token" value="{{ csrf_token()}}" hidden><!-- viewing -->

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Cài đặt hệ thống </h3>
                </div>

                <div class="box-body">
                    @include('flash::message')
                    <div class="animated fadeIn">
                        <div class="col-sm-12">
                            {!! Form::open(['route' => ['settings.store'] ]) !!}
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item active">
                                    <a class="nav-link active" data-toggle="tab" href="#website" role="tab" aria-controls="website" aria-expanded="true"><i class="icon-globe"></i> Website</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#analytic" role="tab" aria-controls="website" aria-expanded="true"><i class="icon-chart"></i> Analytic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-expanded="false"><i class="icon-phone"></i> Liên hệ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-expanded="false"><i class="icon-star"></i> Mạng XH</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#security" role="tab" aria-controls="security" aria-expanded="false"><i class="icon-shield"></i> Bảo mật</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#other" role="tab" aria-controls="other" aria-expanded="false"><i class="icon-settings"></i> Khác</a>
                                </li>
                            </ul>

                            <div class="tab-content mb-4 mt-30">
                                <div class="tab-pane active" id="website" role="tabpanel" aria-expanded="true">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="title">Tiêu đề website (site_name)</label>
                                                <input type="text" class="form-control" id="site_name" name="site_name" value="{{ @$settings['site_name']->value }}" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Từ khóa (keywords)</label>
                                                <input type="text" class="form-control" id="keywords" name="keywords" value="{{ @$settings['keywords']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Mô tả website (description)</label>
                                                <textarea rows="9" class="form-control" id="description" name="description">{{ @$settings['description']->value }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Địa điểm (locale)</label>
                                                <input type="text" class="form-control" id="locale" name="locale" value="{{ @$settings['locale']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="analytic" role="tabpanel" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Google Analytics</label>
                                                <textarea rows="9" class="form-control" id="google_analytics" name="google_analytics">{{ @$settings['google_analytics']->value }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Google Tag Head</label>
                                                <textarea rows="9" class="form-control" id="google_tag_head" name="google_tag_head">{{ @$settings['google_tag_head']->value }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Google Tag Body</label>
                                                <textarea rows="9" class="form-control" id="google_tag_body" name="google_tag_body">{{ @$settings['google_tag_body']->value }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="contact" role="tabpanel" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="title">Email liên hệ (email)</label>
                                                <input type="text" class="form-control" id="email" name="email" value="{{ !empty(@$settings['email']->value) ? @$settings['email']->value : 'admin@gmail.com' }} " required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="title">Hotline (hotline)</label>
                                                <input type="text" class="form-control" id="hotline" name="hotline" value="{{ @$settings['hotline']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="title">Tel (tel)</label>
                                                <input type="text" class="form-control" id="tel" name="tel" value="{{ @$settings['tel']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="title">Địa chỉ (address)</label>
                                                <input type="text" class="form-control" id="address" name="address" value="{{ @$settings['address']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="social" role="tabpanel" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="facebook">Facebook account (facebook_name)</label>
                                                <input type="text" class="form-control" id="facebook_name" name="facebook_name" value="{{ @$settings['facebook_name']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="facebook">Facebook (facebook)</label>
                                                <input type="text" class="form-control" id="facebook" name="facebook" value="{{ @$settings['facebook']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="facebook">Facebook APP Id (facebook_app_id)</label>
                                                <input type="text" class="form-control" id="facebook_app_id" name="facebook_app_id" value="{{ @$settings['facebook_app_id']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="google">Google+ (google)</label>
                                                <input type="text" class="form-control" id="google" name="google" value="{{ @$settings['google']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="instagram">Instagram (instagram)</label>
                                                <input type="text" class="form-control" id="instagram" name="instagram" value="{{ @$settings['instagram']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="youtube">Youtube (youtube)</label>
                                                <input type="text" class="form-control" id="youtube" name="youtube" value="{{ @$settings['youtube']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="zalo">Zalo+ (zalo)</label>
                                                <input type="text" class="form-control" id="zalo" name="zalo" value="{{ @$settings['zalo']->value }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="security" role="tabpanel" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="weakpass">Blacklist mật khẩu (cách nhau dấu ";") (weakpass)</label>
                                                <textarea rows="10" class="form-control" id="weakpass" name="weakpass">123456a@;123456A@;abc@1234;123456</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="other" role="tabpanel" aria-expanded="false">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="title">Phiên bản JS/CSS (version)</label>
                                                <input type="text" class="form-control" id="version" name="version" value="{{!empty(@$settings['version']->value) ? @$settings['version']->value : '1.0' }}" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

