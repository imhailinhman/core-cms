

<!-- Profile Image -->
<div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 col-xs-12">
    <div class="box-body box-profile">
        <h3 class="profile-username text-center">  {!! $new->title !!}</h3>
        <div id="crop-avatar" class="text-center"
             style="background: url('{!! (!empty($new->image)) ? $new->image : config('filepath.no_image') !!}') no-repeat center center; background-size: cover;">
        </div>
        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Danh mục cha : </b> <a class="pull-right">{!! $new->getNameRoles !!}</a>
            </li>
            <li class="list-group-item">
                <b>Keyword Seo : </b> <a class="pull-right">{!! $new->keyword_seo !!}</a>
            </li>
            <li class="list-group-item">
                <b>Tóm tắt : </b> <a class="pull-right">{!! $new->description !!}</a>
            </li>
            <li class="list-group-item">
                <b>Mô tả : </b>
                <div class="clearfix"></div>
                <a class="text-center">{!! $new->content !!}</a>
            </li>

            <li class="list-group-item">
                <b>Trạng thái : </b>
                @if ($new->status == 1)
                    <a class='pull-right btn  btn-xs btn btn-success btn-edit item_actions btn-mg-2'>Kích hoạt</a>
                @else
                    <a class='pull-right btn btn-xs btn-danger  btn-edit item_actions btn-mg-2'>Vô hiệu</a>
                @endif
            </li>

        </ul>
    </div>

</div>
<!-- /.box-body -->
