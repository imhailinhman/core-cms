<div class="form-group col-md-12 col-sm-12 col-xs-12 text-center">
    <img id="img-upload" class="img-fluid center-block" width="200px" height="200px"/>
    <div id="img-upload-remove">
        @if (isset($model->image))
            <img src="{{$model->image }}" class=" img-fluid img-thumbnail center-block "
                 width="200px" height="200px">
            <input type="hidden" name="image_tmp" value="{{$model->image}}"/>
        @else
            <img src="{{ config('filepath.no_image') }}" class="  img-fluid img-thumbnail center-block "
                 width="200px" height="200px">

        @endif
    </div>
    <div class="form-group col-md-offset-5 col-sm-offset-5 col-md-2 col-sm-2 col-xs-12 text-center">
        {!! Html::decode(Form::label('image','Ảnh <span class="required_data">*</span>',['class' => 'font-weight-bold float-left mr-4'])) !!}
        {!! Form::file('image', ['class' => 'form-control w-75' ,'accept' => 'image/*' , 'name' => 'image' ,'placeholder' => 'Ảnh' , 'id' => 'image' ]) !!}
    </div>

</div>


<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Tiêu đề:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('title', (!empty($model)) ? $model->title : old('title'), ['placeholder'=>'Tiêu đề', 'class' => 'form-control']) !!}
</div>


<!-- Keyword Seo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keyword_seo', 'Keyword Seo:') !!}
    {!! Form::text('keyword_seo', (!empty($model)) ? $model->keyword_seo : old('keyword_seo'), ['placeholder'=>'Keyword Seo', 'class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Tóm tắt :') !!}
    {!! Form::textarea('description', (!empty($model)) ? $model->description : old('description'), ['placeholder'=>'Tóm tắt', 'class' => 'form-control', 'id' => '' ]) !!}
</div>


<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Mô tả :') !!}
    {!! Form::textarea('content', (!empty($model)) ? $model->content : old('content'), ['placeholder'=>'Mô Tả visa', 'class' => 'form-control', 'id' => 'editor2' ]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12 col-xs-12 col-md-12">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('visas.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script>
        $('#category_id').select2({
            placeholder: '---Tất cả---'
        });

        $('#formid').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(function () {
            CKEDITOR.replace('editor2',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        })

        $(function () {
            CKEDITOR.replace('editor1',{
                height: '200px',
                language:'vi',
                filebrowserBrowseUrl :'/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                customConfig: '/plugins/ckeditor/config.js'
            });
        })
    </script>
@endsection
