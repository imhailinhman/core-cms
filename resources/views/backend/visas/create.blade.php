@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tạo Visa
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'visas.store', 'enctype' => 'multipart/form-data', 'id' => 'formid' ]) !!}

                        @include('backend.visas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
