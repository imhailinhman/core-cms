@extends('backend.layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <input id="data-token" value="{{ csrf_token()}}" type="hidden">
                <input id="data-link-delete-item-actions" value="{{ route("role.destroy") }}" hidden>

                <form action="" method="get" id="form_custom_list">
                    <div class="div_option row">
                        <div class="col-xs-12 col-md-3 col-sm-3 ">
                            <div class="form-group">
                                <input id="filter-name" class="form-control search_name enter-submit" name="filter_name"
                                       placeholder="Tim kiếm theo tên quyền  ..." value="{{ request('filter_name') }}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 col-sm-3 ">
                            <div class="form-group">
                                <select name="active" class="form-control" id="active">
                                    <option>--- Lọc theo trạng thái ---</option>
                                    <option value="1" {{ (request('active') == '1') ? 'selected="selected"' : '' }}>Kích hoạt</option>
                                    <option value="0" {{ (request('active') == '0') ? 'selected="selected"' : '' }}>Vô hiệu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 col-sm-3">
                            <div class="div_option wrap-filters ">
                                <button class="btn btn-success form-group" type="submit">TÌm kiếm </button>
                                <a class="btn btn-default" href="{{ route("role.index") }}">Clear</a>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>


                    <div class="div_option form-group row">
                        <div class="col-xs-3 col-md-6 col-sm-6">
                            <ul class="nav navbar-left panel_toolbox">
                                <li class="dropdown">
                                    <select class="form-control" name="limit" id="limit_pagination">
                                        <option @if (request('limit') == 10)selected="selected" @endif value="10">10</option>
                                        <option @if (request('limit') == 25)selected="selected" @endif value="25">25</option>
                                        <option @if (request('limit') == 50)selected="selected" @endif value="50">50</option>
                                        <option @if (request('limit') == 75)selected="selected" @endif value="75">75</option>
                                        <option @if (request('limit') == 100)selected="selected" @endif value="100">100</option>
                                    </select>
                                </li>
                            </ul>
                        </div>


                        <div class="col-xs-9 col-md-6 col-sm-6 count_record text-right">
                            <a href="javascript:showModalCreateRole()" class="btn btn-success">
                                <i class="fa fa-plus"></i> Thêm mới nhóm quyền
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách quyền</h3>
                    </div>

                    <div class="box-body">
                        @include('flash::message')
                        @include('backend.role.table')
                    </div>
                </div>
                <div class="text-center">

                </div>
            </div>
        </div>
    </div>

    <div id="modal-update-role" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <form method="post" onsubmit="handleSubmitFormUpdateRole(this); return false;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cập nhật nhóm quyền</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('backend.role.form', ['role' => new \App\Models\Role()])
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Lưu</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var request = false;

        function handleSubmitFormUpdateRole(form) {
            // Check id for action update or create
            var modal = $('#modal-update-role');
            var id = $(form).find('[name=id]').eq(0).val();
            var submitBtn = $(form).find('[type=submit]');
            var requestUrl = '';
            if (parseInt(id) > 0) {
                // Update
                requestUrl = '{{ route('role.update') }}';
            } else {
                // Create
                requestUrl = '{{ route('role.create') }}';
            }

            if (requestUrl) {
                submitBtn.prop('disable', true);
                if (request !== false) {
                    request.abort();
                }
                request = $.ajax({
                    url: requestUrl,
                    dataType: 'json',
                    data: $(form).serialize(),
                    type: 'POST',
                    success: function (res) {
                        submitBtn.prop('disable', false);
                        if (res.success) {
                            form.reset();
                            modal.modal('hide');
                            // Reload list role
                            window.location.reload();
                        } else {
                            alert(res.message);
                        }
                    }, error: function (res) {
                        submitBtn.prop('disable', false);
                    }
                });
            }
        }

        function showModalCreateRole() {
            var modal = $('#modal-update-role');

            // Update modal title, show modal
            modal.find('.modal-title').html('Thêm mới nhóm quyền');
            modal.find('[type=submit]').text('Thêm mới');
            modal.find('[name=id]').val(0);
            modal.modal('show');
        }

        function editRole(roleId) {
            var modal = $('#modal-update-role');
            // Get role info
            $.ajax({
                url: '{{ route('role.get_info') }}',
                dataType: 'json',
                type: 'get',
                data: {role_id: roleId},
                success: function(res) {
                    if (res.success) {
                        modal.find('.modal-body').html(res.formHtml);
                        modal.find('.modal-title').html('Cập nhật nhóm quyền');
                        modal.find('[type=submit]').text('Cập nhật');
                        modal.modal('show');
                    } else {
                        alert(res.message);
                    }
                }, error: function (res) {

                }
            });
        }
    </script>
@endsection

