<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="role">
    <thead>
    <tr>
        <th class="text-left">Stt</th>
        <th class="text-left">Tên nhóm quyền</th>
        <th class="text-left">Mô tả</th>
        <th class="text-left">Trạng thái</th>
        <th class="text-left">Action</th>
    </tr>
    </thead>
    <tbody>
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp

    @foreach($roles as $role)
        @php $count_number++ @endphp
        <tr id="tr-{!! $role->id !!}">
            <td align="left">{!! $count_number !!}</td>
            <td align="left">{!! $role->name !!}</td>
            <td align="left">{!! $role->description !!}</td>
            <td class="text-left">
                @if($role->active == \App\Repositories\Interfaces\RoleRepository::STATUS_ACTIVE)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="Status" data-text="active" data-val="0"
                            data-id="{!! $role->id !!}">Kích hoạt
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="Status" data-text="Inactive" data-val="1"
                            data-id="{!! $role->id !!}">Vô hiệu
                    </button>
                @endif

            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="javascript:editRole({{ $role->id }})" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-delete-item-actions" data-key="delete" data-title="role" data-text="delete"
                       data-val="none" data-id="{!! $role->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    {!! $roles->appends(request()->input())->links() !!}
</div>
