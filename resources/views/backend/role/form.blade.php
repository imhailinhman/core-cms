<div class="form-group">
    <label class="col-form-label">Tên nhóm quyền</label>
    <input type="text" class="form-control" name="name" value="{{ $role->name }}">
</div>

<div class="form-group">
    <label class="col-form-label">Nhóm người dùng </label>
    <select name="type" class="form-control type" id="type">
        <option value="none" {!! ($role->type == 'none') ? 'selected="selected"' : '' !!}>--- Nhóm người dùng ---</option>
        <option value="1" {!! ($role->type == '1') ? 'selected="selected"' : '' !!}>admin</option>
        <option value="2" {!! ($role->type == '2') ? 'selected="selected"' : '' !!}>coordinator</option>
        <option value="3" {!! ($role->type == '3') ? 'selected="selected"' : '' !!}>accounts</option>
        <option value="4" {!! ($role->type == '4') ? 'selected="selected"' : '' !!}>shiper</option>
    </select>

</div>

<div class="form-group">
    <label class="col-form-label">Mô tả</label>
    <input type="text" class="form-control" name="description" value="{{ $role->description }}">
</div>
<div class="form-group">
    <label class="col-form-label">Kích hoạt</label>
    <input type="checkbox" class="form-check-input" name="active" value="1" @if($role->active === 1) checked @endif>
</div>
<div class="form-group">
    <label class="col-form-label">Quyền hạn</label>
    <div class="row">
        @php $rolePermission = explode(',', $role->permission) @endphp
        @if(isset($permissions))
            @foreach($permissions as $group => $listPermission)
                <div class="col-md-4">
                    <h5>{{ $group }}</h5>
                    @foreach($listPermission as $permission)
                        <div><input type="checkbox" name="permission[]" value="{{ $permission['id'] }}" @if(in_array($permission['id'], $rolePermission)) checked @endif> {{ $permission['name'] }}</div>
                    @endforeach
                </div>
            @endforeach
        @endif
    </div>
</div>
<input type="hidden" name="id" value="{{ $role->id }}"/>