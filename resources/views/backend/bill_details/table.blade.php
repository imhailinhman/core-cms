<table class="table table-responsive" id="billDetails-table">
    <thead>
        <tr>
            <th>Quantity</th>
        <th>Unit Price</th>
        <th>Product Id</th>
        <th>Bill Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($billDetails as $billDetail)
        <tr>
            <td>{!! $billDetail->quantity !!}</td>
            <td>{!! $billDetail->unit_price !!}</td>
            <td>{!! $billDetail->product_id !!}</td>
            <td>{!! $billDetail->bill_id !!}</td>
            <td>{!! $billDetail->status !!}</td>
            <td>
                {!! Form::open(['route' => ['billDetails.destroy', $billDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('billDetails.show', [$billDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('billDetails.edit', [$billDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>