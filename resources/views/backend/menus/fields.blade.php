<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Tên menu:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('name', (!empty($model)) ? $model->name : old('name'), ['placeholder'=>'Tên menu', 'class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Vị trí hiển thị:') !!}
    <span class="text-danger">(*)</span>
    {!! Form::text('location', (!empty($model)) ? $model->location : old('location'), ['placeholder'=>'vị trí hiển thị', 'class' => 'form-control']) !!}
</div>

{{--<!-- company_id Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('parent', 'Danh mục cha:') !!}--}}
    {{--<span class="text-danger">(*)</span>--}}
    {{--<select name="parent" id="parent" class="form-control select2">--}}
        {{--<option value="">-------- Danh mục --------</option>--}}
        {{--@if(!empty($levels))--}}
            {{--{!! \App\Models\Menu::parentMenu($levels) !!}--}}
        {{--@endif--}}
    {{--</select>--}}
{{--</div>--}}

<!-- Status Field -->
<div class="form-group col-sm-12 col-xs-12 col-md-12">
    {!! Form::label('status', 'Trạng thái :') !!}
    <div class="material-switch float-left">
        {!! Form::hidden('status', false) !!}
        <?php
        $array_atr = ['id' => 'someSwitchOptionSuccess'];
        if (!empty($model) && $model != '') {
            if ($model->status == 1) {
                $array_atr['checked'] = 'checked';
            }
        } else {
            $array_atr['checked'] = 'checked';
        }
        ?>
        {!! Form::checkbox('status', 1, null, $array_atr) !!}
        <label for="someSwitchOptionSuccess" class="label-success"></label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Lưu', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('menus.index') !!}" class="btn btn-default">Cancel</a>
</div>
