@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('menus.index') !!}">Danh sách menu</a></li><li class="active">Cập nhật menu {!! $model->name !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Sửa : {!! $model->fullname !!}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['menus.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data']) !!}

                    @include('backend.menus.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
