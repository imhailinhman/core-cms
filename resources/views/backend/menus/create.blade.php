@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('menus.index') !!}">Quản trị</a></li><li class="active">Tạo mới menu</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Thêm mới menu
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'menus.store', 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true, 'enctype' => 'multipart/form-data']) !!}

                    @include('backend.menus.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
