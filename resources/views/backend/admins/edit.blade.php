@extends('backend.layouts.app')

@section('breadcrumb')
    <li><a href="{!! route('admins.index') !!}">Danh sách quản trị</a></li><li class="active">Cập nhật quản trị {!! $model->fullname !!}</li>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Sửa : {!! $model->fullname !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::open(['route' => ['admins.update', $model->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true , 'enctype' => 'multipart/form-data']) !!}

                        @include('backend.admins.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
