@extends('backend.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Đổi mật khẩu
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        @include('flash::message')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::open(['route' => ['admins.postchangePassword', $admin->id], 'class' => 'form jsvalidation', 'novalidate' => 'novalidate', 'files' => true ]) !!}

                <!-- 'Mật khẩu cũ Field -->
                    <div class="form-group col-lg-offset-4 col-sm-offset-4 col-md-offset-4 col-sm-4 col-md-4 col-xs-12">
                        {!! Form::label('current_password', 'Mật khẩu cũ :',['class' => 'control-label']) !!}
                        <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Mật khẩu cũ">
                    </div>


                    <!-- 'Mật khẩu mới Field -->
                    <div class="form-group col-lg-offset-4 col-sm-offset-4 col-md-offset-4 col-sm-4 col-md-4 col-xs-12">
                        {!! Form::label('password', 'Mật khẩu mới :',['class' => 'control-label']) !!}
                        <input type="password" class="form-control" id="password" name="password" placeholder="Mật khẩu mới">
                    </div>

                    <!-- Nhập lại mật khẩu mớ Field -->
                    <div class="form-group col-lg-offset-4 col-sm-offset-4 col-md-offset-4 col-sm-4 col-md-4 col-xs-12">
                        {!! Form::label('password_confirmation', 'Nhập lại mật khẩu mới :',['class' => 'control-label']) !!}
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Nhập lại mật khẩu mới">
                    </div>
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12 col-xs-12 row text-center">
                        <a href="{!! route('admins.index') !!}" class="btn btn-default">Quay lại </a>
                        {!! Form::submit('Lưu', ['class' => 'btn btn-success']) !!}
                    </div>

                    <div class="clearfix"></div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
