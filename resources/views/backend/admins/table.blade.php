<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline display nowrap w-100"
       cellspacing="0" width="100%" id="example">
    <thead>
    <tr>
        <th class="text-left nosort">
            <label class="mb-0">
                <input type="checkbox" value="" id="select_all" name="select_all">
            </label>
        </th>
        <th class="text-left nosort">Stt</th>
        <th class="text-left nosort">Tên tài khoản</th>
        <th class="text-left nosort">Tên đầy đủ</th>
        <th class="text-left nosort">Quyền</th>
        <th class="text-left nosort">Email</th>
        <th class="text-left nosort">Trạng thái</th>
        <th class="text-left nosort">Hành động</th>
    </tr>
    </thead>
    <tbody class="break-word">
    @php $count_number = (request('page', 1) - 1) * $limit; @endphp
    @foreach($admins as $k => $admin)
        @php
            $count_number++;
        @endphp
        <tr id="tr-{!! $admin->id !!}">
            <td class="text-left p-t-0">
                <div class="checkbox m-t-5">
                    <label>
                        <input class="checkbox_item" type="checkbox" name="checkbox" value="{{$admin->id}}">
                    </label>
                </div>
            </td>
            <td align="left">{!! $count_number !!}</td>
            <td align="left">{!! $admin->username !!}</td>
            <td align="left">{!! $admin->fullname !!}</td>
            <td align="left">{!! $admin->getNameRoles() !!}</td>
            <td align="left">{!! $admin->email !!}</td>
            <td class="text-left">
                @if($admin->status == 1)
                    <button type="button" class="btn_status btn_status_success item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="người dùng" data-text="vô hiệu" data-val="0"
                            data-id="{!! $admin->id !!}">Kích hoạt
                    </button>
                @else
                    <button type="button" class="btn_status btn_status_false item_actions"
                            data-link="data-link-item-actions"
                            data-key="status" data-title="người dùng" data-text="kích hoạt" data-val="1"
                            data-id="{!! $admin->id !!}">Vô hiệu
                    </button>
                @endif
            </td>
            <td align="left">
                <div class='btn-group'>
                    <a href="{!! route('admins.show', [$admin->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admins.edit', [$admin->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>

                    <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-edit item_actions btn-mg-2"
                       data-link="data-link-item-actions" data-key="delete" data-title="người đùng" data-text="Xóa" data-val="none" data-id="{!! $admin->id !!}">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

<div class="pagination-area text-center">
    @if(isset($admins) && !empty($admins))
        {!! $admins->appends(request()->query())->links() !!}
    @endif
</div>
