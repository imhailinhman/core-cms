<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $product->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $product->name !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $product->type !!}</p>
</div>

<!-- Style Field -->
<div class="form-group">
    {!! Form::label('style', 'Style:') !!}
    <p>{!! $product->style !!}</p>
</div>

<!-- Alias Field -->
<div class="form-group">
    {!! Form::label('alias', 'Alias:') !!}
    <p>{!! $product->alias !!}</p>
</div>

<!-- Size Field -->
<div class="form-group">
    {!! Form::label('size', 'Size:') !!}
    <p>{!! $product->size !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $product->image !!}</p>
</div>

<!-- Thumb Field -->
<div class="form-group">
    {!! Form::label('thumb', 'Thumb:') !!}
    <p>{!! $product->thumb !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $product->price !!}</p>
</div>

<!-- Price Sale Field -->
<div class="form-group">
    {!! Form::label('price_sale', 'Price Sale:') !!}
    <p>{!! $product->price_sale !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $product->description !!}</p>
</div>

<!-- Title Seo Field -->
<div class="form-group">
    {!! Form::label('title_seo', 'Title Seo:') !!}
    <p>{!! $product->title_seo !!}</p>
</div>

<!-- Description Seo Field -->
<div class="form-group">
    {!! Form::label('description_seo', 'Description Seo:') !!}
    <p>{!! $product->description_seo !!}</p>
</div>

<!-- Keyword Seo Field -->
<div class="form-group">
    {!! Form::label('keyword_seo', 'Keyword Seo:') !!}
    <p>{!! $product->keyword_seo !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $product->content !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $product->status !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $product->category_id !!}</p>
</div>

<!-- Provider Id Field -->
<div class="form-group">
    {!! Form::label('provider_id', 'Provider Id:') !!}
    <p>{!! $product->provider_id !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $product->country_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $product->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $product->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $product->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $product->deleted_at !!}</p>
</div>

