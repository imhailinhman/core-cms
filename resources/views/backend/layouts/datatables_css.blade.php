<link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
{{--<link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css"/>--}}
<link href="{{ asset('plugins/adminlte/css/adminlte.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/_all-skins/css/_all-skins.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/alt/AdminLTE-select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/datatables.net/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/responsive/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/jquery-loading/src/loading.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/waitMe/css/waitMe.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/toastr/build/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>

<link href="{{ asset('css/mycss.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/theme.min.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">


{{--{!! plugin(--}}
{{--['font-awesome', 'bootstrap','adminlte',--}}
{{--'_all-skins','select2','alt','datatables.net-bs','datatables.net',--}}
{{--'responsive','bootstrap-datepicker','sweetalert','jquery-loading',--}}
{{--'daterangepicker','waitMe','toastr','bootstrap-tagsinput'--}}
{{--]--}}
{{--, 'css') !!}--}}

{{--{!! css(['mycss.css', 'fileinput.min.css', 'theme.min.css']) !!}--}}
{{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">--}}
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />--}}
