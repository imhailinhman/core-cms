<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorys')->insert([
            [
                'id' => 1,
                'title' => 'Tour trong nước',
                'menu_id' => 2,
                'keyword_seo' => 'Tour trong nước',

            ],
            [
                'id' => 2,
                'title' => 'Tour nước ngoài',
                'menu_id' => 2,
                'keyword_seo' => 'Tour nước ngoài',

            ]
        ]);
    }
}
