<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            [
                'id' => 1,
                'district_name' => 'Thanh Xuan',
                'province_id' => '1',
                'status' => true
            ],
            [
                'id' => 2,
                'district_name' => 'Tây Hồ',
                'province_id' => '1',
                'status' => true
            ],
            [
                'id' => 3,
                'district_name' => 'Hoàn Kiếm',
                'province_id' => '1',
                'status' => true
            ],

            [
                'id' => 4,
                'district_name' => 'Quận 1',
                'province_id' => '2',
                'status' => true
            ],

            [
                'id' => 5,
                'district_name' => 'Quận 2',
                'province_id' => '2',
                'status' => true
            ],

 [
                'id' => 6,
                'district_name' => 'Quận 4',
                'province_id' => '2',
                'status' => true
            ],


        ]);
    }
}
