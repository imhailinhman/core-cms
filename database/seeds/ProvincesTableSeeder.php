<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            [
                'id' => 1,
                'province_name' => 'Hà Nội',
                'slug' => 'Ha-noi',
                'status' => true
            ],
            [
                'id' => 2,
                'province_name' => 'Hồ Chí Minh',
                'slug' => 'Ho-chi-minh',
                'status' => true
            ]

        ]);
    }
}
