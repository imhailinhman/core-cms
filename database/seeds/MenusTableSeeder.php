<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            [
                'id' => 1,
                'name' => 'index',
                'location' => 1,
                'level' => 0,
                'parent' => 0,
                'status' => 1,
                'order' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Tour du lịch',
                'location' => 2,
                'level' => 0,
                'parent' => 0,
                'status' => 1,
                'order' => 1,
            ]
        ]);
    }
}
