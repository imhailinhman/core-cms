<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->integer('mobile', false, true)->default(0)->unique();
            $table->string('email', 100)->nullable()->unique();
            $table->string('fullname', 125)->nullable();
            $table->string('address', 125)->nullable();
            $table->string('images', 125)->nullable();
            $table->date('birthdate')->nullable();
            $table->integer('gender', false, true)->default(0)->comment('0: Nam , 1: Nữ , 2 : Giới tính khác');
            $table->integer('status', false, true)->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
