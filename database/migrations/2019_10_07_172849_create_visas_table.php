<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->nullable();
            $table->string('image',255)->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('slug',255)->nullable();
            $table->string('title_seo',255)->nullable();
            $table->string('description_seo',255)->nullable();
            $table->string('keyword_seo',255)->nullable();
            $table->integer('category_id',false,true)->default(0)->unsigned();
            $table->integer('user_id',false,true)->default(0)->unsigned();
            $table->tinyInteger('status')->default(1)->comment('0: Unactive , 1: active');
            $table->tinyInteger('type', false, true)->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visas');
    }
}
