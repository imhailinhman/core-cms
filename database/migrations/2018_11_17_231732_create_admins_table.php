<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('roles')->comment('Multi role, separator by');
            $table->string('email')->unique();
            $table->string('username', 255);
            $table->string('fullname', 255)->nullable();
            $table->string('password');
            $table->string('avatar', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->integer('phone', false, true)->default(0)->unique();
            $table->tinyInteger('status', false, true)->default(1)->comment('0: Unactive , 1: active');
            $table->date('birthday')->nullable()->change();
            $table->tinyInteger('gender', false, true)->default(0)->comment('0: Nam , 1: Nữ , 2 : Giới tính khác');
            $table->rememberToken();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
