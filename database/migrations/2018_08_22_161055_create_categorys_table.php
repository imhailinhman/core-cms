<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id', false, true)->default(0)->unsigned();
            $table->string('title', 255)->nullable();
            $table->string('title_seo', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('description_seo', 255)->nullable();
            $table->string('keyword_seo', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->string('thumb', 255)->nullable();
            $table->string('alias', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->integer('parent_id', false, true)->default(0);
            $table->integer('level', false, true)->default(0);
            $table->integer('sort', false, true)->default(0);
            $table->string('lang' , 3)->nullable();
            $table->tinyInteger('status', false, true)->default(1)->comment('0: Unactive , 1: active');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categorys');
    }
}
