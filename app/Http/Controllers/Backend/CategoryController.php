<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Http\Controllers\AppBaseController;
use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\MenuRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class CategoryController extends AppBaseController
{
    private $categoryRepository;
    private $menuRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        MenuRepository $menuRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->menuRepository = $menuRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['title', 'like', $searchKey, 'OR'],
            ]];
        }

        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }

        $categories = $this->categoryRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.categories.index', compact('limit', 'categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryRepository->all(['*'])->toArray();
        $menus = $this->menuRepository->pluck('name', 'id')->toArray();
        return view('backend.categories.create', compact('menus', 'categories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Category::$rules, Category::$messages);
        $data = $request->only([ 'title', 'description', 'image', 'slug', 'title_seo', 'keyword_seo', 'menu_id', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = categoryRepository::STATUS_DEACTIVE;
        }
        $data['slug'] = Str::slug($data['title'], '-');
        $data['title_seo'] = Str::slug($data['title'], '-');

        try {
            $this->categoryRepository->create($data);
            Flash::success('Taọ mới danh mục thành công! ');
            return redirect(route('categories.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới danh mục thất bại!');
            return redirect(route('categories.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $category = $this->categoryRepository->findById((int)$id);
        if (empty($category)) {
            Flash::error('Người quản trị này không tồn tại!!');
            return redirect(route('backend.categories.index'));
        }
        return view('backend.categories.show', compact(  'category'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $categories = $this->categoryRepository->all(['*'])->toArray();
        $menus = $this->menuRepository->pluck('name', 'id')->toArray();
        $model = $this->categoryRepository->findById((int)$id);
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('categories.index'));
        }
        return view('backend.categories.edit', compact('model', 'menus', 'categories'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Category::$rules, Category::$messages);
        $model = $this->categoryRepository->findById((int)$id);
        //check id category not exist
        if (empty($model)) {
            Flash::error('Tin tức này không tồn tại!!');
            return redirect(route('categories.index'));
        }
        $data = $request->only([ 'title', 'description', 'image', 'slug', 'title_seo', 'keyword_seo', 'menu_id', 'status']);
        if (!isset($data['status'])) {
            $data['status'] = categoryRepository::STATUS_DEACTIVE;
        }
        if (!empty($request->input('title'))) {
            $data['slug'] = Str::slug($data['title'], '-');
            $data['title_seo'] = Str::slug($data['title'], '-');
        }

//        //Update for image
//        if ($request->hasFile('image')) {
//            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'categories');
//        }

        try {
            $this->categoryRepository->update($data, $id);
            Flash::success('Cập nhật danh mục thành công!');
            return redirect(route('categories.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật danh mục thất bại!');
            return redirect(route('categories.index'));
        }
    }
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->categoryRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'danh mục không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
