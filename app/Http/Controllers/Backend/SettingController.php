<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Setting;
use App\Models\Role;
use App\Repositories\Interfaces\SettingRepository;
use App\Repositories\Interfaces\RoleRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Response, URL, Auth;
use Validator;

class SettingController extends AppBaseController
{

    /** @var  SettingRepository */
    private $settingRepository;
    protected $roleRepository;

    /**
     * SettingController constructor.
     * @param SettingRepository $settingRepo
     */
    public function __construct(
        SettingRepository $settingRepo,
        RoleRepository $roleRepository
    )
    {
        $this->settingRepository = $settingRepo;
        $this->roleRepository = $roleRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $settings = $this->settingRepository->all(['*'], [], [], ['id' => 'DESC'])->keyBy('key');
        return view('backend.settings.index', compact( 'settings'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $data = $request->only(['key', 'value']);
        $request = $request->all();
        unset($request['_token']);

        $post_data = [];
        foreach ($request as $key => $value) {
            $data['key'] = $key;
            $data['value'] = $value;
            array_push($post_data, $data);
        }

        try {
            $this->settingRepository->updateSetting($post_data);
            \Cache::forever('settings', $request);
            Flash::success('Giá trị thiết lập đã được lưu! ');
            return redirect(route('settings.index'));
        } catch (\Exception $ex) {
            Flash::error($ex->getMessage());
            return redirect(route('settings.create'));
        }
    }
}
