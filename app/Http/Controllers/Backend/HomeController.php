<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller{

    public function __construct(){
        parent::__construct();
    }

    public function welcome(Request $request)
    {
        return view(
            'backend.home.welcome',
            [
                'message' => 'Welcome Hestia\'s Backend'
            ]
        );
    }
}
