<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UploadHelper;
use App\Facades\Helper;
use App\Http\Controllers\AppBaseController;
use App\Models\Slide;
use App\Repositories\Interfaces\NewRepository;
use App\Repositories\Interfaces\SlideRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Response, URL;
use Validator;

class SlideController extends AppBaseController
{
    private $slideRepository;
    private $categoryRepository;
    private $newRepository;

    public function __construct(
        SlideRepository $slideRepo,
        NewRepository $newRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->slideRepository = $slideRepo;
        $this->categoryRepository = $categoryRepository;
        $this->newRepository = $newRepository;
        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $limit = $request->input('limit', config('system.perPage'));
        //filter
        $filter = [];
        if (!empty($request->input('search_key'))) {
            $searchKey = '%' . $request->input('search_key') . '%';
            $filter[] = [[
                ['title', 'like', $searchKey, 'OR'],
            ]];
        }

        if (!is_null($request->input('filter_status'))) {
            $filter[] = ['status', (int)$request->input('filter_status')];
        }


        $slides = $this->slideRepository->paginateList($limit, $filter, [], ['id' => 'DESC']);
        return view('backend.slides.index', compact('limit', 'slides'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $news = $this->newRepository->pluck('title', 'id')->toArray();
        return view('backend.slides.create', compact('news'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, Slide::$rules, Slide::$messages);
        $data = $request->only([ 'title','image', 'link', 'image', 'product_id', 'status', 'new_id']);
        if (!isset($data['status'])) {
            $data['status'] = SlideRepository::STATUS_DEACTIVE;
        }

        $new_detail = $this->newRepository->findById($data['new_id']);
        if (!empty($new_detail)) {
            $data['title'] = $new_detail['title'];
            $data['link'] = config('app.url').'/'.$new_detail->getSlugNew().'/'.$new_detail['slug'];
        }

        //Update for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'slides');
        }

        try {
            $this->slideRepository->create($data);
            Flash::success('Taọ mới slide thành công! ');
            return redirect(route('slides.index'));
        } catch (\Exception $ex) {
            Flash::error('Tạo mới slide thất bại!');
            return redirect(route('slides.create'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $slide = $this->slideRepository->findById((int)$id);
        if (empty($slide)) {
            Flash::error('Slide này không tồn tại!!');
            return redirect(route('backend.slides.index'));
        }
        return view('backend.slides.show', compact(  'slide'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $news = $this->newRepository->pluck('title', 'id')->toArray();
        $model = $this->slideRepository->findById((int)$id);

        if (empty($model)) {
            Flash::error('Slide này không tồn tại!!');
            return redirect(route('slides.index'));
        }
        return view('backend.slides.edit', compact('model', 'news'));
    }

    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, Slide::$rules, Slide::$messages);
        $model = $this->slideRepository->findById((int)$id);
        //check id slide not exist
        if (empty($model)) {
            Flash::error('Slide này không tồn tại!!');
            return redirect(route('slides.index'));
        }
        $data = $request->only([ 'title','image', 'link', 'product_id', 'status', 'new_id']);
        if (!isset($data['status'])) {
            $data['status'] = SlideRepository::STATUS_DEACTIVE;
        }

        $new_detail = $this->newRepository->findById($data['new_id']);

        if (!empty($new_detail)) {
            $data['title'] = $new_detail['title'];
            $data['link'] = config('app.url').'/'.$new_detail->getSlugNew().'/'.$new_detail['slug'];
        }

        //ate for image
        if ($request->hasFile('image')) {
            $data['image'] = UploadHelper::uploadImgFile($request->file('image'), 'slides');
        }

        try {
            $this->slideRepository->update($data, $id);
            Flash::success('Cập nhật slide thành công!');
            return redirect(route('slides.index'));
        } catch (\Exception $ex) {
            Flash::error('Cập nhật slide thất bại!');
            return redirect(route('slides.index'));
        }
    }

    //action all
    public function bulkActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['ids']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $ids = explode(',', $input['ids']);
        $datas = $this->slideRepository->findByListId($ids);

        if ($datas->isEmpty()) {
            return $this->responseApp(false, 1, 'slide không tồn tại!');
        }
        if ($key == 'delete') {
            foreach ($datas as $item) {
                $item->delete();
            }
        } else if ($key == 'status') {
            foreach ($datas as $item) {
                $item->status = $value;
                $item->save();
            }
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->responseApp(false, 404, 'Thông tin không hợp lệ. Vui lòng thử lại!');
        }

        $key = $input['key'];
        $value = $input['value'];
        $id = $input['id'];

        try {
            $data = $this->slideRepository->findById($id);
        } catch (\Exception $ex) {
            return $this->responseApp(false, 404, 'Người dùng không tồn tại!');
        }

        if ($key == 'delete') {
            $data->delete();
        } else if ($key == 'status') {
            $data->status = $value;
            $data->save();
        }

        return $this->responseApp(true, 200, 'Thành công!');
    }
}
