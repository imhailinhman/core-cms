<?php namespace App\Http\Controllers\Backend;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Flash;

class AuthController extends Controller{

    use DispatchesJobs, ValidatesRequests;


    private $guard;

    const BASE_URL = '/';

    public function __construct(){

//        $this->guard = Auth::guard('admin');
        $this->middleware('guest:admin')->except('logout');
    }


    public function getLogin(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('admins.index'));
        }
        return view('backend.auth.login');
    }

    public function postLogin(Request $request)
    {
        $credentials = [
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ];
        $remember_me = false;
        if($request->has('remember_me') && $request->input('remember_me') == 1)
            $remember_me = true;
        if(Auth::attempt($credentials, $remember_me)) {
            $admin = Auth::user();
            if($admin->status == 0){
                Auth::logout();
                return redirect( route('auth.login') );
            }
            return redirect( route('admins.index') );
        } else {
            Flash::success('Tài khoản hoặc mật khẩu không chính xác!');
            return view('backend.auth.login');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        session()->flush();
        return redirect( route('auth.login') );
    }
}
