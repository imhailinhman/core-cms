<?php

namespace App\Http\Middleware;

use App\Repositories\Interfaces\RoleRepository;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $adminUser = Auth::user();
        $route = $request->route();
        if ($this->roleRepository->count() <= 0) {
            return $next($request);
        }

        if(!$adminUser->canAccess($route->getName())) {
            // Access denied
            if ($request->ajax()) {
                return new Response(json_encode(['success' => false, 'message' => trans('auth.permission_denied')]), 401);
            } else {
                return new Response(view('denied'));
            }
        }
        return $next($request);
    }
}
