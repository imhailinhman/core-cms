<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Cache;

class SettingHelper
{
    public function __construct()
    {
    }

    function setting($key, $default = '')
    {
        static $settings;
        if (is_null($settings)) {
            $settings = Cache::get('settings');
        }

        return \Illuminate\Support\Arr::get($settings, $key, $default);
    }
}
