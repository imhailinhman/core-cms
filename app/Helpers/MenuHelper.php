<?php

namespace App\Helpers;


use App\Repositories\Interfaces\MenuRepository;
use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Support\Facades\Auth;

class MenuHelper
{
    protected $menuRepository;
    protected $categoryRepository;
    public function __construct(
        MenuRepository $menuRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->menuRepository = $menuRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function ListMenu()
    {
        return $this->menuRepository->all(['*'],['status'=> MenuRepository::STATUS_ACTIVE], [], ['location' => 'ASC']);
    }

    public function ListCate($menu_id)
    {
        return $this->categoryRepository->findAllBy('menu_id', (int)$menu_id);
    }
}
