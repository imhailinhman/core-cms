<?php
if (!function_exists('is_secure')) {

    function is_secure()
    {
        return env('APP_SECURE') == 'true';
    }
}