<?php

namespace App\Helpers;

use App\Repositories\Interfaces\SlideRepository;

class SlideHelper
{
    protected $slideRepository;
    public function __construct(
        SlideRepository $slideRepository
    )
    {
        $this->slideRepository = $slideRepository;
    }

    public function ListSlide()
    {
        return $this->slideRepository->all(['*'],['status'=> SlideRepository::STATUS_ACTIVE]);
    }

}
