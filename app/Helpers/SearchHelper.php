<?php

namespace App\Helpers;

use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\ProvinceRepository;

class SearchHelper
{
    protected $categoryRepository;
    protected $provinceRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        ProvinceRepository $provinceRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->provinceRepository = $provinceRepository;
    }

    public function ListCategory()
    {
//        return $this->categoryRepository->pluck('title', 'id')->toArray();
        return $this->categoryRepository->all(['*'],['status'=> CategoryRepository::STATUS_ACTIVE])->pluck('title', 'id')->toArray();
    }

    public function ListProvince()
    {
        return $this->provinceRepository->all(['*'],[])->pluck('province_name', 'id')->toArray();
    }

}
