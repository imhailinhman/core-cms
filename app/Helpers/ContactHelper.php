<?php

namespace App\Helpers;


use App\Repositories\Interfaces\ContactRepository;

class ContactHelper
{
    protected $contactRepository;
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function countUnread()
    {
        return $this->contactRepository->count(['status'=> ContactRepository::STATUS_DEACTIVE]);
    }
}
