<?php

namespace App\Helpers;

use Blade;
use Carbon\Carbon;
use URL;
use File;

class Helper
{
    public function __construct()
    {
    }

    /**
     * formatDate
     * @param $date
     */
    //Y-m-d
    public static function formatDate($date,$format = "d/m/Y"){
        return date($format, strtotime($date));
    }

    /**
     * static enums
     * @access static
     *
     * @param mixed $value
     * @param array $options
     * @param string $default
     * @return string|array
     */
    public static function enum($value, $options, $default = '')
    {
        if ($value !== null) {
            if (array_key_exists($value, $options)) {
                return $options[$value];
            }
            return $default;
        }
        return $options;
    }

    /**
     * Upload file.
     *
     * @return $fileName or false
     */
    public function uploadFile($file, $pathUpload, $fileCus = '-')
    {
        $fileName = time() . $fileCus . $file->getClientOriginalName();
        if (!is_dir($pathUpload)) {
            mkdir($pathUpload, 0777, TRUE);
        }

        if ($file->move($pathUpload, $fileName)) {
            return $fileName;
        }

        return false;
    }

    /**
     * Rename file.
     *
     * @return boolean
     */
    public function renameFile($oldFile, $newFile)
    {
        if (!File::exists($oldFile)) {
            return false;
        }

        rename($oldFile, $newFile);

        return true;
    }

    /**
     * Remove file.
     *
     * @return boolean
     */
    public function removeFile($file, $pathUpload)
    {
        if (!empty($file)) {
            $filePath = $pathUpload . '/' . $file;
            if (!File::exists($filePath)) {
                return false;
            }

            File::delete($filePath);
        }

        return true;
    }

    public function showDiffTime($end_datetime, $start_datetime)
    {
        $diff_seconds = strtotime($end_datetime) - strtotime($start_datetime);
        return $this->showSecondToTime($diff_seconds);
    }

    public function showSecondToTime($seconds_value)
    {
        $hours = 0;
        $minutes = 0;
        if ($seconds_value < 60) {
            $seconds = $seconds_value;
        } else if ($seconds_value < (60 * 60)) {
            // Show in minute
            $minutes = intdiv($seconds_value, 60);
            $seconds = $seconds_value % 60;
        } else {
            // Show by hour
            $hours = intdiv($seconds_value, 60 * 60);
            $seconds_value = $seconds_value % (60*60);
            $minutes = intdiv($seconds_value, 60);
            $seconds = $seconds_value % 60;
        }

        return (string)($hours > 0 ? ($hours < 10 ? '0' . (string)$hours : $hours) . ':' : '') .
            (string)($minutes > 0 ? ($minutes < 10 ? '0' . (string)$minutes : $minutes) : '00') . ':' .
            (string)($seconds > 0 ? ($seconds < 10 ? '0' . (string)$seconds : $seconds) : '00');
    }

    public function showDiffTimeVn($end_datetime, $start_datetime)
    {
        if(empty($end_datetime) || empty($start_datetime)) {
            return 'Chưa xác định thời gian';
        }

        $diff_seconds = strtotime($end_datetime) - strtotime($start_datetime);
        return $this->showSecondToTimeVn($diff_seconds);
    }

    public function showSecondToTimeVn($seconds_value)
    {
        $hours = intdiv($seconds_value, 60 * 60);
        if ($hours < 24) {
            $day = 1;
            $night = 0;
        } elseif ($hours == 24){
            $day = 1;
            $night = 1;
        }else {
            $day = $hours / 24;
            $night = $day - 1;
        }
        if($night == 0) {
            return $day . ' ngày ';
        }
        return $day . ' ngày ' . $night . ' đêm';
    }

}
