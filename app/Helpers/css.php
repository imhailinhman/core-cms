<?php

if (!function_exists('css')) {

    function css($name)
    {
        //get html
        if (!is_array($name)) {
            $name = [$name];
        }

        $css = [];
        foreach ($name as $n) {
            $asset = str_replace(['\\/', '/\\'], '/', "css/{$n}");
            if(strpos($asset, '.css') === false)
                $asset .= ".css";
            $url = asset($asset, is_secure());
            $css[] = "<link href=\"{$url}\" rel=\"stylesheet\" type=\"text/css\"/>";
        }

        return implode("\n", $css);
    }
}