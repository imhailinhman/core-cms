<?php

namespace App\Helpers;


use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class PermissionHelper
{
    public function __construct()
    {

    }

    /**
     * Check current user can access route or not
     * @param $routeName
     * @return bool
     */
    public function canAccess($routeName)
    {
        /* @var $currUser Admin */
        $currUser = Auth::user();
        if ($currUser->canAccess($routeName)) {
            return true;
        }

        return false;
    }

    public function canView($fieldName)
    {
        /* @var $currUser Admin */
        $currUser = Auth::user();
        if ($currUser->canView($fieldName)) {
            return true;
        }

        return false;
    }
}