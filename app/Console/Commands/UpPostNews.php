<?php

namespace App\Console\Commands;

use App\Repositories\Interfaces\NewRepository;
use Illuminate\Console\Command;
use Carbon\Carbon;

class UpPostNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:up-post-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto up post new';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $newRepository;

    public function __construct(NewRepository $newRepository)
    {
        parent::__construct();
        $this->newRepository = $newRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(NewRepository $newRepository)
    {
        $date = Carbon::today()->toDateTimeString();
        $NewsDateNow = $newRepository->all(['*'], [['date_up', '=', $date]]);
        foreach ($NewsDateNow as $new) {
            $newRepository->update(['status' => NewRepository::STATUS_ACTIVE], $new->id);
        }
        $this->info('Đăng bài thành công!');
    return true;
    }
}
