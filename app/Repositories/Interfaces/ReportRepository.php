<?php

namespace App\Repositories\Interfaces;


interface ReportRepository extends BaseRepository
{
    const _TOTAL_ORDERS = 'Tổng số đơn';
    const _TOTAL_ORDERS_CANCEL = 'Tổng số đơn hủy';
    const _TOTAL_ORDERS_SUCCESS = 'Tổng số đơn thành công';

    const _TONG_GIAODICH = 'Tổng giao dịch';
    const _DOANHTHU = 'doanh thu';
    const _PHISHIP = 'phí ship ';
}
