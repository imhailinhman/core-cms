<?php

namespace App\Repositories\Interfaces;


interface BaseRepository
{
    public function count($conditions = []);

    public function all($columns = array('*'), $conditions = [], $relations = [], $orders = []);

    public function paginateList($per = 10, $conditions = [], $relations = [], $orders = []);

    public function pluck($column, $key = null, $sortColumn = null, $direction = 'asc');

    public function findById($id, $columns = array('*'));

    public function create($data);

    public function update($data, $id);

    public function updateOrCreate($conditions, $data);

    public function destroy($id);

    public function findBy($key, $value, $relations = []);

    public function findAllBy($key, $value, $relations = []);

    public function updateAll($array_id, $data);

    public function findByListId($array_id);

    public function insert($array_data);

    public function deleteList($arr_id);

    public function deleteAll();

    public function sum(array $fields, $filters = []);
}