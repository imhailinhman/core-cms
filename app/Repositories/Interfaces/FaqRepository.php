<?php

namespace App\Repositories\Interfaces;


interface FaqRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const TYPE_DEFAUFT = 0;
    const TYPE_PROMOTION = 1;

}
