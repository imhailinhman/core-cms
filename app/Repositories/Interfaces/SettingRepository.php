<?php

namespace App\Repositories\Interfaces;


interface SettingRepository  extends BaseRepository
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const TYPE_DEFAULT = 0;
    const TYPE_PROMOTION = 1;


    function updateSetting($setting_data);
}
