<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbRoleRepository;
use App\Repositories\Interfaces\RoleRepository;

class CacheRoleRepository extends CacheRepository implements RoleRepository
{
    function __construct(DbRoleRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

}