<?php

namespace App\Repositories\Caches;


use Illuminate\Support\Facades\Cache;

class CacheRepository
{
    protected $dbRepository;
    protected $default_cache_time = 60;
    /**
     * CacheRepository constructor.
     * @param $dbRepository
     */
    function __construct($dbRepository)
    {
        $this->dbRepository = $dbRepository;
    }

    public function count($conditions = [])
    {
        return $this->dbRepository->count($conditions);
    }

    public function all($columns = array('*'), $conditions = [], $relations = [], $orders = [])
    {
        return $this->dbRepository->all($columns, $conditions, $relations, $orders);
    }
    public function paginateList($per = 10, $conditions = [], $relations = [], $orders = [])
    {
        return $this->dbRepository->paginateList($per, $conditions, $relations, $orders);
    }
    public function pluck($column, $key = null, $sortColumn = null, $direction = 'asc')
    {
        return $this->dbRepository->pluck($column, $key, $sortColumn, $direction);
    }
    public function findById($id, $columns = array('*'))
    {
        return $this->dbRepository->findById($id, $columns);
    }
    public function create($data)
    {
        return $this->dbRepository->create($data);
    }
    public function update($data, $id)
    {
        $obj = $this->dbRepository->findById($id);
        return $obj->update($data);
    }
    public function updateOrCreate($conditions, $data)
    {
        return $this->dbRepository->updateOrCreate($conditions, $data);
    }
    public function destroy($id)
    {
        $obj = $this->dbRepository->findById($id);
        return $obj->delete();
    }
    public function findBy($key, $value, $relations = [])
    {
        return $this->dbRepository->findBy($key, $value, $relations);
    }
    public function findAllBy($key, $value, $relations = [])
    {
        return $this->dbRepository->findAllBy($key, $value, $relations);
    }

    public function updateAll($array_id, $data)
    {
        return $this->dbRepository->updateAll($array_id, $data);
    }

    public function findByListId($array_id)
    {
        $cache_key = strtolower($this->dbRepository->getModelName()) . '_find_by_list_' . md5(implode(',', $array_id));
        return Cache::remember($cache_key, 1, function () use ($array_id) {
            return $this->dbRepository->findByListId($array_id);
        });
    }

    public function insert($array_data)
    {
        return $this->dbRepository->insert($array_data);
    }

    public function deleteList($arr_id)
    {
        return $this->dbRepository->deleteList($arr_id);
    }

    public function deleteAll()
    {
        return $this->dbRepository->deleteAll();
    }

    public function updateAllByConditions($conditions, $updateData)
    {
        return $this->dbRepository->updateAllByConditions($conditions, $updateData);
    }

    public function sum(array $fields, $filter = [])
    {
        return $this->dbRepository->sum($fields, $filter);
    }
}
