<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbProductRepository;
use App\Repositories\Interfaces\ProductRepository;

class CacheProductRepository extends CacheRepository implements ProductRepository
{
    function __construct(DbProductRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

    public function listTransport()
    {
        return $this->dbRepository->listTransport();
    }

    public function listType()
    {
        return $this->dbRepository->listType();
    }

    public function listProductByCate($id_cate)
    {
        return $this->dbRepository->listProductByCate($id_cate);
    }

    public function getTitleAndKeyword($slug)
    {
        return $this->dbRepository->getTitleAndKeyword($slug);
    }

}
