<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbProductImageRepository;
use App\Repositories\Interfaces\ProductImageRepository;

class CacheProductImageRepository extends CacheRepository implements ProductImageRepository
{
    function __construct(DbProductImageRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

}
