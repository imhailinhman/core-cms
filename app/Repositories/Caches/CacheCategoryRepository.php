<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbCategoryRepository;
use App\Repositories\Interfaces\CategoryRepository;

class CacheCategoryRepository extends CacheRepository implements CategoryRepository
{
    function __construct(DbCategoryRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }
}
