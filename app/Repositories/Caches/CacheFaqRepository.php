<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbFaqRepository;
use App\Repositories\Interfaces\FaqRepository;

class CacheFaqRepository extends CacheRepository implements FaqRepository
{
    function __construct(DbFaqRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }
}
