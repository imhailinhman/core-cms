<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbUserRepository;
use App\Repositories\Interfaces\UserRepository;

class CacheUserRepository extends CacheRepository implements UserRepository
{
    function __construct(DbUserRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

    /**
     * @param $mobileNumber
     * @return bool
     */
    public function checkExistWithMobileNumber($mobileNumber)
    {
        return $this->dbRepository->checkExistWithMobileNumber($mobileNumber);
    }

    public function findUserByPhone($phoneNumber)
    {
        return $this->dbRepository->findUserByPhone($phoneNumber);
    }
}