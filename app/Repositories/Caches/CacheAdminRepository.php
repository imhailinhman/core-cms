<?php

namespace App\Repositories\Caches;

use App\Repositories\Eloquents\DbAdminRepository;
use App\Repositories\Interfaces\AdminRepository;

class CacheAdminRepository extends CacheRepository implements AdminRepository
{
    function __construct(DbAdminRepository $dbRepository)
    {
        $this->dbRepository = $dbRepository;

    }

}