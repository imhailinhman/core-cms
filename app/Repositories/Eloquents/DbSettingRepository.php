<?php

namespace App\Repositories\Eloquents;


use App\Models\Setting;
use App\Repositories\Interfaces\SettingRepository;

class DbSettingRepository extends DbRepository implements SettingRepository
{
    function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function updateSetting($setting_data)
    {
        \DB::transaction(function () use ($setting_data) {
            foreach ($setting_data as $setting) {
                $this->model->updateOrCreate(['key' => $setting['key']], ['value' => $setting['value']]);
            }
        });
    }
}
