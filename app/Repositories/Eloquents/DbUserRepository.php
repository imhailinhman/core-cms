<?php

namespace App\Repositories\Eloquents;


use App\Models\User;
use App\Repositories\Interfaces\UserRepository;

class DbUserRepository extends DbRepository implements UserRepository
{
    function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param $mobileNumber
     * @return bool
     */
    public function checkExistWithMobileNumber($mobileNumber)
    {
        $user = $this->model->select('id')->where('mobile', $mobileNumber)->orderBy('created_at', 'desc')->first();

        if ($user) {
            return $user->id;
        }
        return false;
    }

    /**
     * @param $phoneNumber
     * @return mixed
     */
    public function findUserByPhone($phoneNumber)
    {
        return $this->model->select('id', 'email', 'fullname', 'address', 'mobile')
            ->where('mobile', 'like', '%'. $phoneNumber .'%')->get();
    }
}