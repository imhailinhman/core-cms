<?php

namespace App\Repositories\Eloquents;


use App\Models\Admin;
use App\Repositories\Interfaces\AdminRepository;

class DbAdminRepository extends DbRepository implements AdminRepository
{
    function __construct(Admin $model)
    {
        $this->model = $model;
    }
}