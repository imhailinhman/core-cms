<?php

namespace App\Repositories\Eloquents;


use App\Models\Role;
use App\Repositories\Interfaces\RoleRepository;

class DbRoleRepository extends DbRepository implements RoleRepository
{
    function __construct(Role $model)
    {
        $this->model = $model;
    }
}