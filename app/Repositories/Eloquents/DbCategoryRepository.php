<?php

namespace App\Repositories\Eloquents;


use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepository;

class DbCategoryRepository extends DbRepository implements CategoryRepository
{
    function __construct(Category $model)
    {
        $this->model = $model;
    }
}
