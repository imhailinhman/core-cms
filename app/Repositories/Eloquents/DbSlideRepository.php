<?php

namespace App\Repositories\Eloquents;


use App\Models\Slide;
use App\Repositories\Interfaces\SlideRepository;

class DbSlideRepository extends DbRepository implements SlideRepository
{
    function __construct(Slide $model)
    {
        $this->model = $model;
    }
}
