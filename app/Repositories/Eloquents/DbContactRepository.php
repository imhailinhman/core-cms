<?php

namespace App\Repositories\Eloquents;


use App\Models\Contact;
use App\Repositories\Interfaces\ContactRepository;

class DbContactRepository extends DbRepository implements ContactRepository
{
    function __construct(Contact $model)
    {
        $this->model = $model;
    }
}
