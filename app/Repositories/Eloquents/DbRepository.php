<?php

namespace App\Repositories\Eloquents;


use Illuminate\Support\Facades\DB;

class DbRepository
{
    /**
     * Eloquent model
     */
    protected $model;
    /**
     * @param $model
     */
    function __construct($model)
    {
        $this->model = $model;
    }

    public function getModelName()
    {
        return class_basename($this->model);
    }

    public function count($conditions = [])
    {
        if (!empty($conditions)) {
            $query = $this->_buildQueryFromFilter($this->model->query(), $conditions);
            return $query->count();
        } else {
            return $this->model->count();
        }
    }

    public function all($columns = array('*'), $conditions = [], $relations = [], $orders = [])
    {
        if (!empty($orders)) {
            $query = $this->model->where('id', '>', 0);
            foreach ($orders as $field => $direction) {
                $query->orderBy($field, $direction);
            }
        } else {
            $query = $this->model->orderBy('id', 'desc');
        }

        if (!empty($conditions)) {
            $query = $this->_buildQueryFromFilter($query, $conditions);
        }

        if (!empty($relations)) {
            $query->with($relations);
        }

        return $query->get($columns);
    }
    public function paginateList($per = 10, $conditions = [], $relations = [], $orders = [])
    {
        if (!empty($orders)) {
            $query = $this->model->where('id', '>', 0);
            foreach ($orders as $field => $direction) {
                $query->orderBy($field, $direction);
            }
        } else {
            $query = $this->model->orderBy('id', 'desc');
        }

        if (!empty($conditions)) {
            $query = $this->_buildQueryFromFilter($query, $conditions);
        }

        if (!empty($relations)) {
            $query->with($relations);
        }

        return $query->paginate($per);
    }
    public function pluck($column, $key = null, $sortColumn = null, $direction = 'asc')
    {
        if (!empty($sortColumn)) {
            return $this->model->orderBy($sortColumn, $direction)->pluck($column, $key);
        } else {
            return $this->model->pluck($column, $key);
        }
    }
    public function findById($id, $columns = array('*'))
    {
        return $this->model->findOrFail($id, $columns);
    }
    public function create($data)
    {
        return $this->model->create($data);
    }
    public function update($data, $id)
    {
        $obj = $this->model->findOrFail($id);
        return $obj->update($data);
    }
    public function updateOrCreate($conditions, $data)
    {
        return $this->model->updateOrCreate($conditions, $data);
    }
    public function destroy($id)
    {
        $obj = $this->model->findOrFail($id);
        return $obj->delete();
    }
    public function findBy($key, $value, $relations = [])
    {
        $query = $this->model->where($key, $value);
        if (!empty($relations)) {
            $query->with($relations);
        }
        return $query->first();
    }
    public function findAllBy($key, $value, $relations = [])
    {
        $query = $this->model->where($key, $value);

        if (!empty($relations)) {
            $query->with($relations);
        }
        return $query->get();
    }

    public function updateAll($array_id, $data)
    {
        return $this->model->whereIn('id', $array_id)->update($data);
    }

    public function findByListId($array_id)
    {
        return $this->model->whereIn('id', $array_id)->get();
    }

    public function insert($array_data)
    {
        return $this->model->insert($array_data);
    }

    public function deleteList($arr_id)
    {
        return $this->model->whereIn('id', $arr_id)->delete();
    }

    public function deleteAll()
    {
        return $this->model->truncate();
    }

    public function updateAllByConditions($conditions, $updateData)
    {
        $query = $this->model->query();
        foreach ($conditions as $key => $value) {
            $query->where($key, $value);
        }
        return $query->update($updateData);
    }

    public function sum(array $fields, $filter = [])
    {
        switch (count($fields)) {
            case 0:
                return 0;
            case 1:
                return $this->model->when($filter, function ($query) use ($filter) {
                    return $this->_buildQueryFromFilter($query, $filter);
                })->sum($fields[0]);
            default:
                $fields = implode(' + ', $fields);
                return $this->model->when($filter, function ($query) use ($filter) {
                    return $this->_buildQueryFromFilter($query, $filter);
                })->sum(DB::raw($fields));
        }
    }

    protected function _buildQueryFromFilter($query, $filter = [])
    {
        if (!empty($filter)) {
            if (isset($filter['in'])) {
                foreach ($filter['in'] as $key => $arr) {
                    $query->whereIn($key, $arr);
                }
                unset($filter['in']);
            }

            if (isset($filter['relations'])) {
                if (!empty($filter['relations'])) {
                    foreach ($filter['relations'] as $relation => $filter2) {
                        if (!empty($filter2)) {
                            $query->whereHas($relation, function ($query) use ($filter2) {
                                $this->_buildQueryFromFilter($query, $filter2);
                            });
                        } else {
                            $query->whereHas($relation);
                        }
                    }
                }
                unset($filter['relations']);
            }
            $query->where($filter);
        }
        return $query;
    }
}
