<?php

namespace App\Repositories\Eloquents;


use App\Models\ProductImage;
use App\Repositories\Interfaces\ProductImageRepository;

class DbProductImageRepository extends DbRepository implements ProductImageRepository
{
    function __construct(ProductImage $model)
    {
        $this->model = $model;
    }
}
