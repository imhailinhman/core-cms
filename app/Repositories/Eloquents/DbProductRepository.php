<?php

namespace App\Repositories\Eloquents;


use App\Models\Product;
use App\Repositories\Interfaces\ProductRepository;

class DbProductRepository extends DbRepository implements ProductRepository
{
    function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function listTransport()
    {
        return [
            ProductRepository::TRANSPORT_CAR => 'Ô tô',
            ProductRepository::TRANSPORT_TRAIN => 'Tàu hỏa',
            ProductRepository::TRANSPORT_PLANE => 'Máy bay',
        ];
    }

    public function listType()
    {
        return [
            ProductRepository::TYPE_DEFAULT => 'Tour thường',
            ProductRepository::TYPE_HOT => 'Điểm đến hấp dẫn',
            ProductRepository::TYPE_FORCUS => 'Điểm đến yêu thích',
            ProductRepository::TYPE_DISCOUNT => 'Tour Khuyến mại',
        ];
    }

    public function listProductByCate($id_cate)
    {
        return $this->model->where('category_id', $id_cate)->get();

    }

    public function getTitleAndKeyword($slug)
    {
        switch ($slug) {
            case 'tour-trong-nuoc':
                $title = 'Du lịch trong nước | Tour trong nước 2019 | Du lịch Việt Nam';
                $keyword = 'du lich trong nuoc, tour trong nuoc';
                break;
            case 'tour-nuoc-ngoai':
                $title = 'Du lịch nước ngoài | Tour nước ngoài 2018 | Du lich nuoc ngoai';
                $keyword = 'du lich nuoc ngoai, tour thai lan, di nuoc ngoai, tour nuoc ngoai';
                break;
            default:
                $title = 'Du lịch trong nước | Tour trong nước 2019 | Du lịch Việt Nam';
                $keyword = 'du lich trong nuoc, tour trong nuoc';
                break;
        }

        return['title' => $title , 'keyword' => $keyword];
    }
}
