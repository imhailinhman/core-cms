<?php

namespace App\Repositories\Eloquents;


use App\Models\Faq;
use App\Repositories\Interfaces\FaqRepository;

class DbFaqRepository extends DbRepository implements FaqRepository
{
    function __construct(Faq $model)
    {
        $this->model = $model;
    }

}
