<?php

namespace App\Repositories\Eloquents;


use App\Models\News;
use App\Repositories\Interfaces\NewRepository;

class DbNewRepository extends DbRepository implements NewRepository
{
    function __construct(News $model)
    {
        $this->model = $model;
    }

}
