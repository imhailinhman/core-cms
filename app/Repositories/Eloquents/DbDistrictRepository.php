<?php

namespace App\Repositories\Eloquents;


use App\Models\District;
use App\Repositories\Interfaces\DistrictRepository;
use Illuminate\Support\Facades\DB;

class DbDistrictRepository extends DbRepository implements DistrictRepository
{
    function __construct(District $model)
    {
        $this->model = $model;
    }

    /**
     * @param $slug
     * @param $province_id
     * @return bool|mixed
     */
    public function getIdBySlug($slug, $province_id)
    {
        $item = $this->model->select('id')
            ->where('province_id', $province_id)
            ->where(function ($query) use ($slug) {
                $query->where('slug', $slug)
                    ->orWhere(DB::raw('REPLACE(slug, "-", "")'), $slug);
            })
            ->first();
        if ($item) {
            return $item->id;
        } else {
            return false;
        }
    }
}