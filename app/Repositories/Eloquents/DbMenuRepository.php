<?php

namespace App\Repositories\Eloquents;


use App\Models\Menu;
use App\Repositories\Interfaces\MenuRepository;

class DbMenuRepository extends DbRepository implements MenuRepository
{
    function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function parentMenu($data, $parent = 0, $str="--", $select = 0)
    {
        foreach ($data as $val ) {
            $id = $val['id'];
            $name = $val['name'];
            if($val['parent'] == $parent) {
                if($select != 0 && $id = $select) {
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'> $str $name</option>";
                }
                $this->parentMenu($data, $id, $str."--");
            }
        }
    }
}
