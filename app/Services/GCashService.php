<?php

namespace App\Services;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class GCashService
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    protected $serviceUrl;
    protected $serviceVersion;

    public function __construct()
    {
        $this->serviceUrl = env('GCASH_SERVICE_URL', 'http://api-gcash.g-pay.vn');
        $this->serviceVersion = env('GCASH_SERVICE_VERSION', '');
    }

    private function sentRequest($url, $params, $method)
    {
        $client = new Client();
        $url = $this->serviceUrl . $url;

        $options = [];

        switch ($method) {
            case 'POST':
                $options['form_params'] = $params;
                break;
            case 'GET':
                $options['query'] = $params;
                break;
            default:
                $options['form_params'] = $params;
                break;
        }

        return $client->request($method, $url, $options);
    }

    public function getPrices($price, $type)
    {
        $url = '/cms/fee';
        $result = $this->processResponse($this->sentRequest($url, ['price' => $price, 'type' => $type], self::METHOD_POST));

        $response = [
            'price' => $price,
            'fee' => 0,
            'bonus' => 0,
            'extra' => 0
        ];
        if ($result['success']) {
            $response['fee'] = $result['data']['fee'];
            $response['bonus'] = $result['data']['bonus'];
            $response['extra'] = $result['data']['extra'];
        }

        return $response;
    }

    public function createOrderSession($orderId)
    {
        $url = '/cms/session';
        $result = $this->processResponse($this->sentRequest($url, ['id' => $orderId], self::METHOD_POST));

        $response = [
            'session' => ''
        ];
        if ($result['success']) {
            $response['session'] = $result['data']['session'];
        }

        return $response;
    }

    /**
     * @param $orderId
     * @return array
     */
    public function sentPushOrderCreated($orderId)
    {
        $url = '/cms/call';
        $result = $this->processResponse($this->sentRequest($url, ['id' => $orderId], self::METHOD_POST));

        return $result;
    }

    /**
     * @param $orderId
     * @return array
     */
    public function sentPushOrderCancelled($orderId)
    {
        $url = '/cms/order/cancel';
        $result = $this->processResponse($this->sentRequest($url, ['id' => $orderId], self::METHOD_POST));

        return $result;
    }

    /**
     * @param $orderId
     * @return array
     */
    public function sentNotify($orderId)
    {
        $url = '/cms/notify';
        return $this->processResponse($this->sentRequest($url, ['id' => $orderId], self::METHOD_POST));
    }

    /**
     * @param $orderId
     * @return array
     */
    public function sentMoneyNotify($orderId)
    {
        $url = '/cms/money/notify';
        return $this->processResponse($this->sentRequest($url, ['id' => $orderId], self::METHOD_POST));
    }

    /**
     * @param $response
     * @return array
     */
    protected function processResponse($response)
    {
        switch ($response->getStatusCode()) {
            case 200:
                $body = $response->getBody()->getContents();
                if (!empty($body)) {
                    $data = json_decode($body, true);
                    return ['success' => true, 'data' => $data];
                } else {
                    Log::error('[GCashService] Error: ' . json_encode($response));
                    return ['success' => false, 'error_code' => 1, 'message' => 'Invalid response'];
                }
                break;
            default:
                Log::error('[GCashService] Error: ' . json_encode($response));
                return ['success' => false, 'error_code' => $response->getStatusCode(), 'message' => 'Request failed'];
                break;
        }
    }
}