<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Menu
 * @package App\Models
 * @version December 3, 2018, 10:50 pm +07
 *
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property string name
 * @property integer parent
 * @property string route
 * @property integer order
 * @property boolean status
 */
class Slide extends Model
{
    use SoftDeletes;

    public $table = 'slides';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'link',
        'image',
        'product_id',
        'new_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'title' => 'required',
    ];

    public static $messages = [
//        'title.required' => 'Tên menu là trường bắt buộc!',
    ];


    public function categories()
    {
        return $this->hasMany(\App\Models\Category::class);
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    public function new()
    {
        return $this->belongsTo(\App\Models\News::class);
    }

}
