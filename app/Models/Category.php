<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version December 3, 2018, 10:47 pm +07
 *
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property \Illuminate\Database\Eloquent\Collection News
 * @property \Illuminate\Database\Eloquent\Collection Product
 * @property \Illuminate\Database\Eloquent\Collection Recruitment
 * @property string title
 * @property string title_seo
 * @property string description
 * @property string description_seo
 * @property string keyword_seo
 * @property string avatar
 * @property string thumb
 * @property string alias
 * @property string slug
 * @property integer parent_id
 * @property integer home
 * @property integer hot
 * @property integer focus
 * @property integer sort
 * @property string lang
 * @property boolean status
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categorys';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'menu_id',
        'title',
        'title_seo',
        'description',
        'description_seo',
        'keyword_seo',
        'image',
        'thumb',
        'alias',
        'slug',
        'parent_id',
        'home',
        'hot',
        'focus',
        'sort',
        'lang',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_seo' => 'string',
        'description' => 'string',
        'description_seo' => 'string',
        'keyword_seo' => 'string',
        'image' => 'string',
        'thumb' => 'string',
        'alias' => 'string',
        'slug' => 'string',
        'parent_id' => 'integer',
        'home' => 'integer',
        'hot' => 'integer',
        'focus' => 'integer',
        'sort' => 'integer',
        'lang' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'title' => 'required|max:255',
        'menu_id' => 'required',
        'image' => 'mimes:jpeg,bmp,png|max:2048|image',
    ];


    public static $messages = [
        'title.required' => 'Tiêu đề là trường bắt buộc!',
        'title.max' => 'Tiêu đề tối đa 255!',
        'menu_id.required' => 'Menu là trường bắt buộc!',
        'image.mimes' => 'Ảnh không đúng định dạng !',
        'image.max' => 'Ảnh vượt quá tối đa kích thước!',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function news()
    {
        return $this->hasMany(\App\Models\News::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function recruitments()
    {
        return $this->hasMany(\App\Models\Recruitment::class);
    }

    public function menu()
    {
        return $this->belongsTo(\App\Models\Menu::class);
    }

    public static function parentCate($data, $parent_id = 0, $str="--", $select = 0)
    {
        foreach ($data as $val ) {
            $id = $val['id'];
            $name = $val['title'];
            if($val['parent_id'] == $parent_id) {
                if($select != 0 && $id = $select) {
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
                static::parentCate($data, $id, $str."--");
            }
        }
    }

}
