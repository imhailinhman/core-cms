<?php

namespace App\Models;


class Permission
{
    protected static $permissions = [
        1 => [
            'group' => '--Quản lý người dùng--',
            'name' => 'Quản lý người dùng',
            'routes' => ['admins.index', 'admins.create', 'admins.store', 'admins.edit', 'admins.update', 'admins.show', 'admins.bulkActions', 'admins.itemActions'],
        ],
        2 => [
            'group' => '--Quản lý hệ thống--',
            'name' => 'Quản lý phân quyền',
            'routes' => ['role.index', 'role.store', 'role.update'],
        ],
        3 => [
            'group' => '--Quản lý người dùng--',
            'name' => 'Quản lý khách hàng',
            'routes' => ['users.index', 'users.create', 'users.store', 'users.edit', 'users.update', 'users.show', 'users.bulkActions', 'users.itemActions'],
        ],

    ];

    protected static function getCollectionData()
    {
        $items = [];

        foreach (self::$permissions as $id => $permission) {
            $permission['id'] = $id;
            $items[] = collect($permission);
        }

        return collect($items);
    }

    /**
     * List permission, grouped by group
     */
    public static function listPermissionByGroup()
    {
        return self::getCollectionData()->groupBy('group');
    }

    protected static function listPermissionByRouteAndId()
    {
        $data = [];

        foreach (self::$permissions as $id => $permission) {
            foreach ($permission['routes'] as $route) {
                $data[$route] = $id;
            }
        }

        return $data;
    }

    /**
     * Check a route name is in permissions or not
     *
     * @param $routeName
     * @return bool|mixed
     */
    public static function isInPermission($routeName)
    {
        $permissions = self::listPermissionByRouteAndId();
        if (in_array($routeName, array_keys($permissions))) {
            return $permissions[$routeName];
        }

        return false;
    }
}
