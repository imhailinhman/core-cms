<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Recruitment
 * @package App\Models
 * @version December 3, 2018, 10:56 pm +07
 *
 * @property \App\Models\Category category
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property string position
 * @property string alias
 * @property string quantity
 * @property string type
 * @property string experience
 * @property string salary
 * @property string diploma
 * @property string description
 * @property string benefit
 * @property string requirement
 * @property string profile
 * @property string image
 * @property string thumb
 * @property string|\Carbon\Carbon time_out
 * @property string|\Carbon\Carbon time
 * @property integer category_id
 * @property boolean status
 */
class Recruitment extends Model
{
    use SoftDeletes;

    public $table = 'recruitment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'position',
        'alias',
        'quantity',
        'type',
        'experience',
        'salary',
        'diploma',
        'description',
        'benefit',
        'requirement',
        'profile',
        'image',
        'thumb',
        'time_out',
        'time',
        'category_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'position' => 'string',
        'alias' => 'string',
        'quantity' => 'string',
        'type' => 'string',
        'experience' => 'string',
        'salary' => 'string',
        'diploma' => 'string',
        'description' => 'string',
        'benefit' => 'string',
        'requirement' => 'string',
        'profile' => 'string',
        'image' => 'string',
        'thumb' => 'string',
        'category_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }
}
