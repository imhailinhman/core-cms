<?php

namespace App\Models;

use App\Repositories\Interfaces\RoleRepository;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


/**
 * Class Admin
 * @package App\Models
 * @version October 24, 2018, 3:52 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property \Illuminate\Database\Eloquent\Collection promotionUses
 * @property string username
 * @property string password
 * @property string fullname
 * @property boolean status
 * @property string email
 * @property string avatar
 */
class Admin extends Authenticatable
{
    use Notifiable;

    public $table = 'admins';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
//    protected $dates = ['deleted_at'];

    public $fillable = [
        'username',
        'password',
        'fullname',
        'roles',
        'status',
        'avatar',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'password' => 'string',
        'fullname' => 'string',
        'status' => 'string',
        'avatar' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules_edit = [
        'username' => 'required|max:255|min:3|without_spaces',
        'roles' => 'required',
        'fullname' => 'max:255',
        'email' => 'required|email|max:255',
        'avatar' => 'mimes:jpeg,bmp,png|max:2048|image',
    ];

    public static $messages_edit = [
        'username.required' => 'Username là trường bắt buộc!',
        'username.max' => 'Username tối đa 255!',
        'username.without_spaces' => 'Tài khoản không chứa dấu cách!',
        'roles.required' => 'Quyền là trường bắt buộc!',
        'fullname.max' => 'Tên đầy đủ tối đa 255!',
        'email.required' => 'Email là trường bắt buộc!',
        'email.max' => 'Email tối đa 255!',
        'email.email' => 'Email không đúng định dạng !',
    ];

    public static $rules_cr = [
        'username' => 'required|max:255|unique:admins|without_spaces',
        'fullname' => 'max:255',
        'roles' => 'required',
        'password' => 'required|max:255',
        'email' => 'required|email|max:255|unique:admins',
        'avatar' => 'mimes:jpeg,bmp,png|max:2048|image',
    ];


    public static $messages_cr = [
        'username.required' => 'Tài khoản là trường bắt buộc!',
        'username.without_spaces' => 'Tài khoản không chứa dấu cách!',
        'username.Regex' => 'Tài khoản không chứa!',
        'roles.required' => 'Quyền là trường bắt buộc!',
        'username.max' => 'Tài khoản tối đa 255!',
        'username.min' => 'Tài khoản tối thiểu 3!',
        'username.unique' => 'Tài khoản đã tồn tại!',
        'password.required' => 'Password là trường bắt buộc!',
        'password.max' => 'Password tối đa 255!',
        'fullname.max' => 'Tên đầy đủ tối đa 255!',
        'email.required' => 'Email là trường bắt buộc!',
        'email.max' => 'Email tối đa 255!',
        'email.email' => 'Email không đúng định dạng !',
        'avatar.mimes' => 'Ảnh không đúng định dạng !',
        'avatar.max' => 'Ảnh vượt quá tối đa kích thước!',

    ];

    public static $rules_change = [
        'current_password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',
    ];

    public static $messages_change = [
        'current_password.required' => 'Mật khẩu hiện tại là trường bắt buộc!',
        'password.required' => 'Mật khẩu mới là trường bắt buộc!',
        'password_confirmation.required' => 'Mật khẩu xác nhận là trường bắt buộc!',
        'password_confirmation.same' => 'Mật khẩu xác nhận và mật khẩu mới không khớp!',
    ];

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    /**
     * Return array permission id of admin account
     */
    public function permissions()
    {
        $roleRepository = App::make(RoleRepository::class);
        $roleIds = explode(',', $this->roles);
        // Get list permission id
        $rolePermissions = $roleRepository->findByListId($roleIds)->pluck('permission');
        if ($rolePermissions) {
            $permissionIds = [];
            foreach ($rolePermissions as $permissions) {
                $permissionIds += explode(',', $permissions);
            }

            return $permissionIds;
        }

        return [];
    }

    public function canAccess($routeName)
    {
        // Get list permission actions
        $permissionId = Permission::isInPermission($routeName);

        if ($permissionId !== false) {
            // Check is admin

            // Check role permissions
            $accountPermissions = $this->permissions();
            if (empty($accountPermissions) || !in_array($permissionId, $accountPermissions)) {
                return false;
            }
        }

        return true;
    }

    public function getNameRoles()
    {
        $role_name = "";
        if ($this->roles) {
            $roleIds = explode(',', $this->roles);
            foreach($roleIds as $k => $roleId) {
                $role = DB::table('roles')->where('active' , 1)->where('id' , $roleId)->get()->toArray();
                if (!empty($role)){
                    $role_n = $role[0]->name;
                    if(!next($roleIds)){
                        $role_name .= $role_n;
                    }else{
                        $role_name .= $role_n . ' , '  ;
                    }
                }else{
                    $role_name = 'Chưa được cấp quyền';
                }
            }
            return $role_name;
        }
    }
}
