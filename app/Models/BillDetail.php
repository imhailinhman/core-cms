<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BillDetail
 * @package App\Models
 * @version December 3, 2018, 11:01 pm +07
 *
 * @property \App\Models\Bill bill
 * @property \App\Models\Product product
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property integer quantity
 * @property float unit_price
 * @property integer product_id
 * @property integer bill_id
 * @property boolean status
 */
class BillDetail extends Model
{
    use SoftDeletes;

    public $table = 'bill_detail';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'quantity',
        'unit_price',
        'product_id',
        'bill_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'quantity' => 'integer',
        'unit_price' => 'float',
        'product_id' => 'integer',
        'bill_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bill()
    {
        return $this->belongsTo(\App\Models\Bill::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
}
