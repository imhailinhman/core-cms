<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Menu
 * @package App\Models
 * @version December 3, 2018, 10:50 pm +07
 *
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property string name
 * @property integer parent
 * @property string route
 * @property integer order
 * @property boolean status
 */
class Menu extends Model
{
    use SoftDeletes;

    public $table = 'menus';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'location',
        'level',
        'parent',
        'route',
        'order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'location' => 'required|unique:menus',
    ];

    public static $messages = [
        'name.required' => 'Tên menu là trường bắt buộc!',
        'location.required' => 'Vị trí đặt menu là trường bắt buộc!',
        'location.unique' => 'Vị trí đặt menu đã tồn tại!',
    ];

    public static $rules_edit = [
        'name' => 'required',
        'location' => 'required',
    ];

    public static $messages_edit = [
        'name.required' => 'Tên menu là trường bắt buộc!',
        'location.required' => 'Vị trí đặt menu là trường bắt buộc!',
    ];


    public function categories()
    {
        return $this->hasMany(\App\Models\Category::class);
    }

    public static function parentMenu($data, $parent = 0, $str="--", $select = 0)
    {
        foreach ($data as $val ) {
            $id = $val['id'];
            $name = $val['name'];
            if($val['parent'] == $parent) {
                if($select != 0 && $id = $select) {
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
                static::parentMenu($data, $id, $str."--");
            }
        }
    }

    public static function parentViewMenu($parent)
    {
        $name_menu = DB::table('menus')->where('id', $parent)->first();
        if(!empty($name_menu)) {
            return $name_menu->name;
        }
        return 'Danh mục cha';

    }

}
