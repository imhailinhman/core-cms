<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
//use Illuminate\Database\Eloquent\SoftDeletes;s
class User extends Model implements AuthenticatableContract, CanResetPasswordContract{

    use Authenticatable, CanResetPassword;
    //    use SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

//    protected $dates = ['birthdate'];

//    protected $dates = ['deleted_at'];


    public $fillable = [
        'phone',
        'password',
        'email',
        'fullname',
        'identity',
//        'birthdate',
        'gender',
        'address',
        'avatar',
        'access_token',
        'access_token_time',
        'last_login',
        'status',
        'created_by',
        'created_on'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone' => 'string',
        'password' => 'string',
        'email' => 'string',
        'fullname' => 'string',
        'identity' => 'string',
        'gender' => 'string',
        'address' => 'string',
        'avatar' => 'string',
        'access_token' => 'string',
        'status' => 'string',
        'created_by' => 'string',
        'created_on' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules_cr = [
        'gender' => 'required',
        'phone' => 'required|digits_between:10,10|numeric|unique:users',
        'fullname' => 'max:255',
        'password' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
    ];


    public static $messages_cr = [
        'gender.required' => 'Giới tính là trường bắt buộc!',
        'phone.required' => 'Số điện thoại là trường bắt buộc!',
        'phone.number' => 'Số điện thoại phải là số!',
        'phone.unique' => 'Số điện thoại này đã tồn tại !',
        'phone.digits_between' => 'Số điện thoại gồm 10 kí tự !',
        'password.required' => 'Password là trường bắt buộc!',
        'password.max' => 'Password tối đa 255!',
        'fullname.max' => 'Tên đầy đủ tối đa 255!',
        'email.required' => 'Email là trường bắt buộc!',
        'email.max' => 'Email tối đa 255!',
        'email.email' => 'Email không đúng định dạng!',
    ];




    public static $rules_edit = [
        'gender' => 'required',
        'phone' => 'required|numeric',
        'fullname' => 'max:255',
        'password' => 'max:255',
        'email' => 'required|email|max:255',
    ];


    public static $messages_edit = [
        'gender.required' => 'Giới tính là trường bắt buộc!',
        'phone.required' => 'Số điện thoại là trường bắt buộc!',
        'phone.number' => 'Số điện thoại phải là số!',
        'phone.unique' => 'Số điện thoại này đã tồn tại !',
        'password.max' => 'Password tối đa 255!',
        'fullname.max' => 'Tên đầy đủ tối đa 255!',
        'email.required' => 'Email là trường bắt buộc!',
        'email.max' => 'Email tối đa 255!',
        'email.email' => 'Email không đúng định dạng!',
    ];

    public function news()
    {
        return $this->hasMany(\App\Models\News::class);
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    //Giới tính
    const GENDER_NAM = 1;
    const GENDER_NU = 0;
    const GENDER_OTHER = 2;

    /**
     * static enum: Model::function()
     *
     * @access static
     * @param integer|null $value
     * @return string|array
     */
    public static function genders($value = null)
    {
        if ($value == self::GENDER_NAM){
            return 'Nam';
        }elseif (!is_null($value) && $value == self::GENDER_NU){
            return 'Nữ';
        }else{
            return 'Giới tính khác';
        }
    }
}
