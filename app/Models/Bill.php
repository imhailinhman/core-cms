<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Bill
 * @package App\Models
 * @version December 3, 2018, 11:02 pm +07
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection BillDetail
 * @property \Illuminate\Database\Eloquent\Collection news
 * @property string total
 * @property string|\Carbon\Carbon date_order
 * @property string payment
 * @property string note
 * @property string size
 * @property integer user_id
 * @property boolean status
 */
class Bill extends Model
{
    use SoftDeletes;

    public $table = 'bills';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'total',
        'date_order',
        'payment',
        'note',
        'size',
        'user_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total' => 'string',
        'payment' => 'string',
        'note' => 'string',
        'size' => 'string',
        'user_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function billDetails()
    {
        return $this->hasMany(\App\Models\BillDetail::class);
    }
}
