<?php

namespace App\Models;

use App\Repositories\Interfaces\VisaRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Visa
 * @package App\Models
 * @version December 3, 2018, 10:53 pm +07
 *
 * @property \App\Models\Category category
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection billDetail
 * @property string title
 * @property string description
 * @property string image
 * @property string thumb
 * @property string content
 * @property string tags
 * @property string slug
 * @property string title_seo
 * @property string description_seo
 * @property string keyword_seo
 * @property integer category_id
 * @property integer user_id
 * @property boolean status
 */
class Visas extends Model
{
    use SoftDeletes;

    public $table = 'visas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'image',
        'description',
        'content',
        'slug',
        'title_seo',
        'description_seo',
        'keyword_seo',
        'category_id',
        'user_id',
        'status',
        'type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
        'title' => 'required|max:255',
    ];


    public static $messages = [
        'title.required' => 'Tiêu đề là trường bắt buộc!',
        'title.max' => 'Tiêu đề tối đa 255!',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
//    public function user()
//    {
//        return $this->belongsTo(\App\Models\User::class);
//    }

    public function users()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }

    public static function typeVisa()
    {
        return [
          VisaRepository::TYPE_DEFAULT => 'Tin tức mặc định',
          VisaRepository::TYPE_PROMOTION => 'Tin tức khuyến mại',
        ];
    }

    public function getTypeVisaText()
    {
        switch ($this->type) {
            case VisaRepository::TYPE_DEFAULT:
                return 'tin tức mặc định';
                break;
            case VisaRepository::TYPE_PROMOTION:
                return 'Tin tức khuyến mại';
                break;
            default:
                return 'tin tức mặc định';
                break;
        }
    }

    public function getSlugVisa()
    {
        switch ($this->type) {
            case VisaRepository::TYPE_DEFAULT:
                return 'tin-tuc';
                break;
            case VisaRepository::TYPE_PROMOTION:
                return 'chuong-trinh-khuyen-mai';
                break;
            default:
                return 'tin-tuc';
                break;
        }
    }

}
