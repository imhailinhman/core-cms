<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Province
 * @package App\Models
 * @version October 30, 2018, 4:49 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection District
 * @property \Illuminate\Database\Eloquent\Collection payments
 * @property \Illuminate\Database\Eloquent\Collection promotionUses
 * @property string province_name
 * @property boolean status
 */
class Province extends Model
{
    public $table = 'provinces';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


//    protected $dates = ['deleted_at'];


    public $fillable = [
        'province_name',
        'status',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'province_name' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function districts()
    {
        return $this->hasMany(\App\Models\District::class);
    }
}
